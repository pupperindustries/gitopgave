﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Interface for all damagable objects
    /// </summary>
    interface IDamagable
    {
        /// <summary>
        /// Called when the idamagable takes damage
        /// </summary>
        void TakeDamage(float damage);
        /// <summary>
        /// Should return the position of this interface
        /// </summary>
        Vector GetPosition();
        /// <summary>
        /// Should return current health
        /// </summary>
        float GetCurrentHealth();
        /// <summary>
        /// Should return max health
        /// </summary>
        float GetMaxHealth();
    }
}
