﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    /// <summary>
    /// A unit in the game
    /// </summary>
    abstract class Unit : GameObject, IDamagable
    {
        /// <summary>
        /// The GameObject / location the unit is currently assigned to
        /// </summary>
        protected IslandLocation assignedLocation;

        /// <summary>
        /// The GameObject/location the unit is currently moving towards
        /// </summary>
        protected IslandLocation currentTargetLocation;

        protected bool canSelect;
        /// <summary>
        /// True if this unit can be selected by clicking on it
        /// </summary>
        public bool CanSelect { get { return canSelect; } }
        /// <summary>
        /// This unit's speed in game units/second
        /// </summary>
        protected float UnitSpeed;
        /// <summary>
        /// The amount of health this unit has.
        /// </summary>
        private float health;
        /// <summary>
        /// The maximum amount of health this unit can have
        /// </summary>
        private float maxHealth;
        /// <summary>
        /// The ui health indicator for this unit
        /// </summary>
        private UIHealthIndicator uiHealthIndicator;
        public UIHealthIndicator uIHealthIndicator { get { return uiHealthIndicator; } }

        /// <summary>
        /// The minimum random distance to a point on its path
        /// </summary>
        protected float minPatrolRadius;
        /// <summary>
        /// The subtarget to move to during its patrol
        /// </summary>
        private Vector movementTarget;
        
        /// <summary>
        /// The name of the sound to make when getting hit by a bullet
        /// </summary>
        private string hitSound;
        /// <summary>
        /// The name of the sound to make when selected
        /// </summary>
        private string selectSound;
        /// <summary>
        /// True if the unit has already died
        /// </summary>
        private bool isDead;

        /// <summary>
        /// Used for locking assingnment target
        /// </summary>
        protected readonly object assignmentLock = new object();

        public Unit(Vector position, float health, string selectSound, string hitSound) : base(null, position, Vector.One, 0)
        {
            this.health = health;
            this.maxHealth = health;
            this.uiHealthIndicator = new UIHealthIndicator(this, 11, 20f, (this is EnemySoldier));
            Engine.I.AddUI(uiHealthIndicator);
            movementTarget = position;
            this.hitSound = hitSound;
            this.selectSound = selectSound;
            isDead = false;
            layer = GameObjectLayer.Unit;
        }

        public abstract void AssignLocation(IslandLocation location);

        public void CheckInput()
        {
            if (canSelect && IsClicked())
            {
                Audio.Play(selectSound);
                SelectorObject.I.SelectUnit(this);
            }
        }

        protected override void Update()
        {
            Move();
            CheckInput();
            base.Update();
        }
        private void CheckDeath()
        {
            if(health <= 0 && isDead != true)
            {
                OnDeath();
            }
        }
        /// <summary>
        /// Called when the unit has 0 or less health
        /// </summary>
        protected virtual void OnDeath()
        {
            Engine.I.RemoveGameObject(this);
            isDead = true;
        }
        /// <summary>
        /// Makes this unit take damage
        /// </summary>
        public void TakeDamage(float damage)
        {
            Audio.Play(hitSound, false, 0.1f);
            health -= damage;
            CheckDeath();
        }

        public Vector GetPosition()
        {
            return Position;
        }

        public float GetCurrentHealth()
        {
            return health;
        }

        public float GetMaxHealth()
        {
            return maxHealth;
        }
        public override void OnRemove()
        {
            Engine.I.RemoveUI(uiHealthIndicator);
            base.OnRemove();
        }
        /// <summary>
        /// Move toward the current target and make subtargets underway
        /// </summary>
        private void Move()
        {
            if (assignedLocation == null)
                return;
            //Vector towards the target
            Vector targetVector = movementTarget - Position;
            //How long will I move this frame?
            float frameDistance = UnitSpeed * Engine.DeltaTime;
            //Move the distance
            Position += targetVector.Normalized * frameDistance;
            //If the distance to move this frame was shorter than my remaining distance,
            if (frameDistance >= targetVector.Length)
            {
                //Calculate a new subtarget
                CalculateSubtarget();
            }
            //Fix rotation to look in the way I'm moving
            float targetRotation = Vector.DiferenceVectorAngle(movementTarget, Position);
            Rotation = Rotation.LerpAngle(targetRotation, 10 * Engine.DeltaTime);
        }
        public virtual void TargetLocationArrival()
        {

        }
        /// <summary>
        /// Calculate a subtarget to patrol and move irregularly toward the target destination
        /// </summary>
        public void CalculateSubtarget()
        {
            if (currentTargetLocation == null)
                return;
            //Vector towards my final target
            Vector targetVector = currentTargetLocation.Position - Position;
            //What's the halfway point?
            Vector half = Position + (targetVector * 0.5f);
            //Take the halfway point, and add a random direction to it.
            movementTarget = half + (Rng.InsideUnitCircle * Math.Max(targetVector.Length * 0.25f, minPatrolRadius));
            //If my new subtarget is closer than 0.5f to the final target,
            if(Vector.Distance(movementTarget, currentTargetLocation.Position) < 0.5f)
            {
                //Signal that I've arrived there.
                TargetLocationArrival();
            }
        }
    }
}
