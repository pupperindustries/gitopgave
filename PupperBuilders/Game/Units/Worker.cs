﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    /// <summary>
    /// A worker
    /// </summary>
    class Worker : Unit
    {
        /// <summary>
        /// The resource the worker is currently holding
        /// </summary>
        private GatheringResource currentlyHolding;

        private static string[] spritenames = new string[] { "gabe", "quinn", "sheryl" };

        public Worker(Vector position) : base(position, 50f, "dogSelect", "dogDeath")
        {

            string chosenName = spritenames[Rng.NextInt(0, spritenames.Length)];

            Sprite = AssetManager.GetImage("Dogs/" + chosenName + ".png");
            MatchAspectRatio(true);
            this.Position = position;

            currentlyHolding = GatheringResource.None;
            assignedLocation = null;
            currentTargetLocation = null;
            canSelect = true;
            UnitSpeed = 0.8f;
        }

        /// <summary>
        /// Determines what happens when a worker arrives at their target location
        /// </summary>
        /// <param name="location">The location a worker is currently arriving at</param>
        public override void TargetLocationArrival()
        {
            lock (currentTargetLocation)
            {
                if (currentTargetLocation is Resource)
                {
                    Resource resourceLocation = currentTargetLocation as Resource;
                    currentlyHolding = resourceLocation.LocalResource;
                    currentTargetLocation = MainBuilding.I;
                }
                else if (currentTargetLocation is MainBuilding)
                {
                    ReturnToMain(currentlyHolding);
                    currentlyHolding = GatheringResource.None;
                    currentTargetLocation = assignedLocation;
                }
            }
        }

        /// <summary>
        /// What happens when a worker returns to the main building
        /// </summary>
        /// <param name="resource"></param>
        public void ReturnToMain(GatheringResource resource)
        {
            switch (resource)
            {
                case GatheringResource.Gold:
                    GameWorld.Gold += GameWorld.WorkerGoldCapacity;
                    break;
                case GatheringResource.Treats:
                    GameWorld.Treats += GameWorld.WorkerTreatCapacity;
                    break;
                default:
                    break;
            }
            Audio.Play("gainMoney");
        }

        public override void AssignLocation(IslandLocation location)
        {
            if (!(location is Building))
            {
                Audio.Play("dogSelect");
                lock (assignmentLock)
                {
                    assignedLocation = location;
                    currentTargetLocation = location;
                }
            }
        }
        
        protected override void Update()
        {
            base.Update();
        }
    }
}
