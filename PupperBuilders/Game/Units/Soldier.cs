﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    /// <summary>
    /// A soldier
    /// </summary>
    abstract class Soldier : Unit
    {
        /// <summary>
        /// When the soldier is allowed to shoot again
        /// </summary>
        private float nextShootTime;
        /// <summary>
        /// How much time must pass between shots for this unit
        /// </summary>
        protected float shootDelay;
        /// <summary>
        /// How much damage this unit deals with one bullet
        /// </summary>
        protected float damage;
        /// <summary>
        /// the name of the sound to play when shooting
        /// </summary>
        private string shootSound;
        public Soldier(Vector position, float shootDelay, float damage, float health, string selectSound, string hitSound, string shootSound) : base(position, health, selectSound, hitSound)
        {
            nextShootTime = 0f;
            this.shootDelay = shootDelay;
            this.damage = damage;
            minPatrolRadius = 2f;
            this.shootSound = shootSound;
        }
        protected override void Update()
        {
            CheckShooting();
            base.Update();
        }
        /// <summary>
        /// Shoots the specified target unit
        /// </summary>
        protected void ShootUnit(IDamagable target)
        {
            if (nextShootTime > Engine.TotalTime)
                return;
            Audio.Play(shootSound, false, 0.25f);
            Bullet bullet = new Bullet(Position, target, damage);
            nextShootTime = Engine.TotalTime + shootDelay;
            Engine.I.AddGameObject(bullet);
        }
        /// <summary>
        /// Called to allow the soldier to choose a target to shoot
        /// </summary>
        protected abstract void CheckShooting();
        

        public override void AssignLocation(IslandLocation location)
        {
            lock (assignmentLock)
            {
                assignedLocation = location;
                currentTargetLocation = location;
            }
        }
    }

}
