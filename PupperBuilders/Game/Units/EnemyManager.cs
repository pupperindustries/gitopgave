﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Class responsible for running a thread, which spawns enemies
    /// </summary>
    static class EnemyManager
    {
        private static Thread thread;

        /// <summary>
        /// The current wave level
        /// </summary>
        private static int levelIndex;
        public static int LevelIndex { get { return levelIndex; } }

        /// <summary>
        /// A list of leve
        /// </summary>
        private static List<Level> levels;

        public static void Start()
        {
            SetupLevels();
            Abort();
            levelIndex = 0;
            thread = new Thread(GameThread);
            thread.Start();
        }

        public static void Abort()
        {
            if (thread != null)
            {
                thread.Abort();
                thread = null;
            }
        }

        private static void GameThread()
        {
            while (true)
            {
                Level current = levels[Math.Min(levelIndex, levels.Count - 1)];
                float timeToWait = current.WaitTime / 1000f;
                float beforeTime = GameWorld.PlayTime;
                while (GameWorld.PlayTime - beforeTime < timeToWait)
                {
                    Thread.Sleep((int)(((beforeTime + timeToWait) - GameWorld.PlayTime)) * 1000);
                }
                EnemyShip ship = new EnemyShip(current.GroupCount, current.GroupSize);
                Engine.I.AddGameObject(ship);

                levelIndex++;
            }
        }

        private static void SetupLevels()
        {
            levels = new List<Level>();
            levels.Add(new Level(45, 1, 1));
            levels.Add(new Level(60, 1, 2));
            levels.Add(new Level(60, 1, 2));
            levels.Add(new Level(60, 2, 1));
            levels.Add(new Level(60, 2, 2));
            levels.Add(new Level(60, 1, 2));
            levels.Add(new Level(60, 1, 1));
            levels.Add(new Level(60, 1, 3));
            levels.Add(new Level(60, 2, 1));
            levels.Add(new Level(60, 1, 2));
            levels.Add(new Level(60, 1, 2));
            levels.Add(new Level(60, 1, 2));
            levels.Add(new Level(60, 1, 3));
            levels.Add(new Level(60, 2, 2));
            levels.Add(new Level(60, 3, 1));
            levels.Add(new Level(60, 3, 2));
            levels.Add(new Level(60, 2, 1));
            levels.Add(new Level(60, 2, 2));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 2, 3));
            levels.Add(new Level(60, 1, 7));
            levels.Add(new Level(60, 2, 4));
            levels.Add(new Level(60, 5, 2));
            levels.Add(new Level(60, 3, 3));
        }

        /// <summary>
        /// Contains information about an attack wave
        /// </summary>
        private class Level
        {
            public Level(int waitTime, int groupCount, int groupSize)
            {
                this.waitTime = waitTime * 1000;
                this.groupCount = groupCount;
                this.groupSize = groupSize;
            }

            private int waitTime;
            /// <summary>
            /// The amount of seconds before the next wave
            /// </summary>
            public int WaitTime
            {
                get
                {
                    return waitTime;
                }
            }
            private int groupCount;
            /// <summary>
            /// The amount of enemy groups to spawn
            /// </summary>
            public int GroupCount
            {
                get
                {
                    return groupCount;
                }
            }
            private int groupSize;
            /// <summary>
            /// The amount of enemies to spawn in each group
            /// </summary>
            public int GroupSize
            {
                get
                {
                    return groupSize;
                }
            }
        }
    }
}
