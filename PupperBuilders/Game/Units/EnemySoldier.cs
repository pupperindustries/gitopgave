﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// An enemy soldier
    /// </summary>
    class EnemySoldier : Soldier
    {
        /// <summary>
        /// How far the enemy can shoot
        /// </summary>
        private const float range = 2.5f;
        public EnemySoldier(Vector position) : base(position, 2f, 5f, 50f, "", "catDeath", "catShot")
        {
            Sprite = AssetManager.GetImage("Cats/Cat"+Rng.NextInt(1,15)+".png");
            MatchAspectRatio(true);
            UnitSpeed = 1f;
        }
        protected override void CheckShooting()
        {
            //Find potential target and loop through each to find a target to shoot
            GameObject[] playerSoldiers = Engine.GameObjectsWhere(gameObject => gameObject is PlayerSoldier);
            GameObject[] workers = Engine.GameObjectsWhere(gameObject => gameObject is Worker);
            GameObject[] buildings = Engine.GameObjectsWhere(gameObject => gameObject is Building);
            GameObject[] mainBuildings = Engine.GameObjectsWhere(gameObject => gameObject is MainBuilding);
            foreach (GameObject mainBuilding in mainBuildings)
            {
                if (Vector.Distance(mainBuilding.Position, Position) > range)
                    continue;
                ShootUnit(mainBuilding as MainBuilding);
                return;
            }
            foreach (GameObject building in buildings)
            {
                if (Vector.Distance(building.Position, Position) > range)
                    continue;
                ShootUnit(building as Building);
                return;
            }
            foreach (GameObject worker in workers)
            {
                if (Vector.Distance(worker.Position, Position) > range)
                    continue;
                ShootUnit(worker as Worker);
                return;
            }
            foreach (GameObject playerSolider in playerSoldiers)
            {
                if (Vector.Distance(playerSolider.Position, Position) > range)
                    continue;
                ShootUnit(playerSolider as PlayerSoldier);
                return;
            }
        }
    }
}
