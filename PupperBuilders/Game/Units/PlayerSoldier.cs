﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// A player soldier
    /// </summary>
    class PlayerSoldier : Soldier
    {
        /// <summary>
        /// The range of the soldier
        /// </summary>
        private const float range = 3f;
        public PlayerSoldier(Vector position) : base(position, 1.5f, 10f, 100f, "dogSelect", "dogDeath", "dogShot")
        {
            Sprite = AssetManager.GetImage("Dogs/woofers.png");
            MatchAspectRatio(true);
            UnitSpeed = 1.25f;
            currentTargetLocation = MainBuilding.I;
            AssignLocation(currentTargetLocation);
            canSelect = true;
        }

        protected override void CheckShooting()
        {
            GameObject[] enemies = Engine.GameObjectsWhere(gameObject => gameObject is EnemySoldier);
            foreach(GameObject enemy in enemies)
            {
                if (Vector.Distance(enemy.Position, Position) > range)
                    continue;
                ShootUnit(enemy as EnemySoldier);
                return;
            }
        }
    }
}
