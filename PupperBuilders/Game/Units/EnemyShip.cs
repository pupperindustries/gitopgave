﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// An enemy ship that spawns enemies
    /// </summary>
    class EnemyShip : GameObject
    {
        /// <summary>
        /// If true, the ship kills itself when reaching its destination
        /// </summary>
        private bool killAfterMoving;
        /// <summary>
        /// Where the ship is sailing to
        /// </summary>
        private Vector targetPosition;
        /// <summary>
        /// The direction the ship is moving
        /// </summary>
        private Vector moveDir;
        /// <summary>
        /// How many units per second the ship moves
        /// </summary>
        private float speed = 2;
        /// <summary>
        /// How many enemies to spawn in each group
        /// </summary>
        private int groupSize;
        /// <summary>
        /// How many groups of enemies to spawn
        /// </summary>
        private int groupCount;

        public EnemyShip(int groupCount, int groupSize) : base(AssetManager.GetImage("Cats/ship.png"), Vector.Zero, new Vector(3f, 1.5f), 0)
        {
            this.groupSize = groupSize;
            this.groupCount = groupCount;
        }

        protected override void Start()
        {
            Side fromSide = (Side)Rng.NextInt(0, 4);
            switch (fromSide)
            {
                case Side.Left:
                    moveDir = Vector.Right;
                    targetPosition = new Vector(-Island.IslandSize / 2, Rng.NextFloat(-Island.IslandSize / 2, Island.IslandSize / 2));
                    break;
                case Side.Right:
                    moveDir = Vector.Left;
                    targetPosition = new Vector(Island.IslandSize / 2, Rng.NextFloat(-Island.IslandSize / 2, Island.IslandSize / 2));
                    Rotation = 180;
                    break;
                case Side.Up:
                    moveDir = Vector.Up;
                    targetPosition = new Vector(Rng.NextFloat(-Island.IslandSize / 2, Island.IslandSize / 2), Island.IslandSize / 2);
                    Rotation = -90;
                    break;
                case Side.Down:
                    moveDir = Vector.Down;
                    targetPosition = new Vector(Rng.NextFloat(-Island.IslandSize / 2, Island.IslandSize / 2), -Island.IslandSize / 2);
                    Rotation = 90;
                    break;
                default:
                    break;
            }
            Position = targetPosition + -moveDir * 12;
        }

        protected override void Update()
        {
            float distanceToTarget = Vector.Distance(Position, targetPosition);
            if (distanceToTarget < .1f)
            {
                if (killAfterMoving)
                {
                    Engine.I.RemoveGameObject(this);
                }
                else
                {
                    for (int i = 0; i < groupCount; i++)
                    {
                        GameObject[] attackablePlaces = Engine.GameObjectsWhere(go => go is IslandLocation);
                        GameObject attackingPlace = attackablePlaces[Rng.NextInt(0, attackablePlaces.Length)];
                        for (int j = 0; j < groupSize; j++)
                        {
                            Vector moveDirRotated90 = moveDir;
                            moveDirRotated90.Rotate(90);
                            EnemySoldier soldier = new EnemySoldier(targetPosition + moveDir * Rng.NextFloat(0, 1f) + moveDirRotated90 * Rng.NextFloat(-1f, 1f));
                            soldier.AssignLocation(attackingPlace as IslandLocation);
                            Engine.I.AddGameObject(soldier);
                        }
                    }

                    Position = targetPosition;
                    targetPosition = -moveDir * Island.IslandSize * 2;
                    moveDir = -moveDir;
                    Rotation += 180;
                    Audio.Play("thud2");
                    Audio.Play("woodBreak2");
                    killAfterMoving = true;
                }
            }
            else
            {
                float frameMovement = speed * Engine.DeltaTime;
                Position += moveDir * Math.Min(distanceToTarget, frameMovement);
            }
        }
    }
}
