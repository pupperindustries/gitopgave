﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    class ShipMerrill : GameObject
    {
        private static Vector startPosition = new Vector(8.4f, -4.1f);
        private static Vector endPosition = new Vector(20f, -4.1f);
        private bool moveOut;
        private float t;
        private float waitTimeIn, waitTimeOut;

        public ShipMerrill() : base(AssetManager.GetImage("ship_merrill.png"), startPosition, new Vector(2f, 1f), 0)
        {
            moveOut = true;
        }

        protected override void Update()
        {
            base.Update();
            if (waitTimeIn >= 0)
            {
                Rotation = 0;
                waitTimeIn -= Engine.DeltaTime;
            }
            else if (waitTimeOut >= 0)
            {
                waitTimeOut -= Engine.DeltaTime;
            }
            else
            {
                if (moveOut)
                {
                    Rotation = 0;
                    t += Engine.DeltaTime * 0.2f;
                    if (t >= 1)
                    {
                        moveOut = false;
                        waitTimeOut = 30;
                    }
                }
                else
                {
                    Rotation = 180;
                    t -= Engine.DeltaTime * 0.2f;
                    if (t <= 0)
                    {
                        moveOut = true;
                        waitTimeIn = 20;
                    }
                }
                Position = Vector.Lerp(startPosition, endPosition, t);
            }
        }
    }
}
