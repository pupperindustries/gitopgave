﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Water background
    /// </summary>
    class Water : GameObject
    {
        public Water() : base(AssetManager.GetImage("water.png"), Vector.Zero, Vector.One * 50, 0f)
        {
            layer = GameObjectLayer.Background;
        }
    }
}
