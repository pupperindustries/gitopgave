﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    class IslandLocation : GameObject
    {
        private UIIslandLocationName uiName;

        public IslandLocation(Image sprite, Vector position, Vector size, float rotation, string name, Vector nameOffset) : base(sprite, position, size, rotation)
        {
            uiName = new UIIslandLocationName(this, name, nameOffset);
            Engine.I.AddUI(uiName);
        }
    
        /// <summary>
        /// Checks if this location is clicked and selects it on SelectorObject
        /// </summary>
        public void CheckInput()
        {
            if (IsClicked())
            {
                SelectorObject.I.SelectLocation(this);
            }
        }

        protected override void Update()
        {
            CheckInput();
            base.Update();
        }
    }


}
