﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    class TradeSchool : Building
    {
        public static int buildingPrice = 100;
        private static TradeSchool i;
        public static TradeSchool I { get { return i; } }

        public TradeSchool() : base(AssetManager.GetImage("Buildings/gold.png"), new Vector(5, 0), Vector.One * 1.3f, 200, "Trade School")
        {
            i = this;
        }

        protected override void OnBuildingDesctruction()
        {
            i = null;
            base.OnBuildingDesctruction();
        }

    }
}
