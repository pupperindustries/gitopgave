﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    class TreatStorage : Building
    {
        public static int buildingPrice = 100;
        private static TreatStorage i;
        public static TreatStorage I { get { return i; } }
        public static int goodboyes;

        public TreatStorage() : base(AssetManager.GetImage("Buildings/treats.png"), new Vector(-5f, 0), (Vector.One * 1.2f), 200, "Treat Storage")
        {
            i = this;
            goodboyes = 2;
        }
    }
}
