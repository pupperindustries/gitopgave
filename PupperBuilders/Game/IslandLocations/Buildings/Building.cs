﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// A building
    /// </summary>
    abstract class Building : IslandLocation, IDamagable
    {
        protected int buildingLevel;
        /// <summary>
        /// The upgrade level of this building
        /// </summary>
        public int BuildingLevel { get { return buildingLevel; } }
        protected int upgradeGoldCost;
        protected int upgradeTreatsCost;
        /// <summary>
        /// The amount of gold required to upgrade this building
        /// </summary>
        public int UpgradeGoldCost { get { return upgradeGoldCost * buildingLevel; } }
        /// <summary>
        /// The amount of treats required to upgrade this building
        /// </summary>
        public int UpgradeTreatsCost { get { return upgradeTreatsCost * (buildingLevel * BuildingLevel); } }
        /// </summary>
        /// The max amount of health this building can have
        /// <summary>
        protected float maxHealth;
        /// <summary>
        /// The amount of health this building hase
        /// </summary>
        protected float health;
        /// <summary>
        /// The ui health indicator for this building
        /// </summary>
        private UIHealthIndicator uiHealthIndicator;

        public Building(Image sprite, Vector position, Vector size, float maxHealth, string name) : base(sprite, position, size, 0, name, new Vector(0, 1))
        {
            buildingLevel = 1;
            this.maxHealth = maxHealth;
            this.health = maxHealth;
            uiHealthIndicator = new UIHealthIndicator(this, 15, 30f, false);
            Engine.I.AddUI(uiHealthIndicator);

            upgradeGoldCost = 50;
            upgradeTreatsCost = 0;

        }
        /// <summary>
        /// Upgrades the building if the player can afford it.
        /// </summary>
        public virtual void UpgradeBuilding()
        {
            if (GameWorld.Gold >= UpgradeGoldCost)
            {
                GameWorld.Gold -= UpgradeGoldCost;
                health = maxHealth;
                buildingLevel++;
                Audio.Play("upgradeSound");
                MainBuilding.I.SetupMenu();
            }
            else
            {
                Audio.Play("click");
            }
        }
        /// <summary>
        /// Downgrades the building.
        /// </summary>
        public void DowngradeBuilding()
        {
            buildingLevel--;
            health = maxHealth;
        }
        public void TakeDamage(float damage)
        {
            health -= damage;
            if (health <= 0)
            {
                OnDepletedHP();
            }
        }
        /// <summary>
        /// Called when the building dies
        /// </summary>
        protected virtual void OnDepletedHP()
        {
            if (buildingLevel > 1)
            {
                DowngradeBuilding();
            }
            else
            {
                OnBuildingDesctruction();
            }
        }

        /// <summary>
        /// What happens when a building has to be removed from the world
        /// </summary>
        protected virtual void OnBuildingDesctruction()
        {
            Engine.I.RemoveGameObject(this);
        }

        public Vector GetPosition()
        {
            return Position;
        }
        public float GetMaxHealth()
        {
            return maxHealth;
        }
        public float GetCurrentHealth()
        {
            return health;
        }
        public override void OnRemove()
        {
            Engine.I.RemoveUI(uiHealthIndicator);
            base.OnRemove();
        }

        public string PriceText()
        {
            string text;
            if (UpgradeTreatsCost != 0)
            {
                text = "Gold: " + UpgradeGoldCost + ", Treats: " + UpgradeTreatsCost;
            }
            else
            {
                text = "(" + UpgradeGoldCost + "g)";
            }
            return text;
        }
    }
}
