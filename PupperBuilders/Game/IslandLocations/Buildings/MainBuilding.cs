﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    /// <summary>
    /// The main building
    /// </summary>
    class MainBuilding : Building
    { 
        /// <summary>
        /// The amount of treats necessary to recruit a new worker
        /// </summary>
        private int workerCost;
        public static int WorkerCost
        {
            get
            {
                return I.workerCost;
            }
        }

        /// <summary>
        /// The amount of treats necessary to recruit a new soldier
        /// </summary>
        private int soldierCost;

        private static MainBuilding i;
        public static MainBuilding I { get { return i; } }

        public MainBuilding() : base(AssetManager.GetImage("Townhall/townhall_1.png"), Vector.Zero, Vector.One * 1.8f, 250f, "Town Hall")
        {
            i = this;
            workerCost = 5;
            soldierCost = 3;
            buildingLevel = 1;
            upgradeGoldCost = 100;
            upgradeTreatsCost = 25;
            health = maxHealth;
            SetupMenu();
        }

        public void SetupMenu()
        {
            int buildingFontSize = 12;
            int unitFontSize = 13;
            List<UISideMenuButton> buttons = new List<UISideMenuButton>();
            buttons.Add(new UISideMenuButton("Buy Worker \n(" + workerCost + " treats)", BuyWorker, unitFontSize));
            buttons.Add(new UISideMenuButton("Buy Soldier \n(" + soldierCost + " treats)" , BuySoldier, unitFontSize));
            if (Barracks.I == null)
            {
                buttons.Add(new UISideMenuButton("Buy Barracks (" + Barracks.buildingPrice + "g)" , BuyBaracks, buildingFontSize));
            }
            else
            {
                buttons.Add(new UISideMenuButton("Upgrade Barracks \n" + Barracks.I.PriceText(), Barracks.I.UpgradeBuilding, buildingFontSize));
            }
            if (WorkerHousing.I == null)
            {
                buttons.Add(new UISideMenuButton("Buy Housing (" + WorkerHousing.buildingPrice + "g)", BuyWorkerHousing, buildingFontSize));
            }
            else
            {
                buttons.Add(new UISideMenuButton("Upgrade Housing \n" + WorkerHousing.I.PriceText(), WorkerHousing.I.UpgradeBuilding, buildingFontSize));
            }
            if (TradeSchool.I == null)
            {
                buttons.Add(new UISideMenuButton("Buy Trade School (" + TradeSchool.buildingPrice + "g)", BuyTradeSchool, buildingFontSize));
            }
            else
            {
                buttons.Add(new UISideMenuButton("Upgrade Trade School \n" + TradeSchool.I.PriceText(), TradeSchool.I.UpgradeBuilding, buildingFontSize));
            }
            if (TreatStorage.I == null)
            {
                buttons.Add(new UISideMenuButton("Buy Treat Storage (" + TreatStorage.buildingPrice + "g)", BuyTreatStorage, buildingFontSize));
            }
            else
            {
                buttons.Add(new UISideMenuButton("Upgrade Treat Storage \n" + TreatStorage.I.PriceText(), TreatStorage.I.UpgradeBuilding, buildingFontSize));
            }
            if (BuildingLevel < 4)
                buttons.Add(new UISideMenuButton("Upgrade Townhall \n" + PriceText(), UpgradeBuilding, buildingFontSize));

            //buttons.Add(new UISideMenuButton("Levi Test", LeviTest));
            UISideMenu.SetPanelData(buttons);
        }

        private void LeviTest()
        {
            TakeDamage(5000);
        }

        public void BuyWorker()
        {
            if (GameWorld.Treats >= workerCost && GameWorld.WorkerCapacity > GameWorld.CurrentWorkerCount)
            {
                Vector spawnPosition = Position + Rng.OnUnitCircle * 1.5f;
                Worker worker = new Worker(spawnPosition);
                Engine.I.AddGameObject(worker);
                SelectorObject.I.SelectUnit(worker);
                GameWorld.Treats -= workerCost;
                Audio.Play("newUnit");
            }
            else
            {
                Audio.Play("click");
            }
        }

        public void BuySoldier()
        {
            if (GameWorld.CurrentSoldierCount < GameWorld.SoldierCapacity && GameWorld.Treats >= soldierCost)
            {
                PlayerSoldier soldier = new PlayerSoldier(Vector.Zero);
                Engine.I.AddGameObject(soldier);
                SelectorObject.I.SelectUnit(soldier);
                GameWorld.Treats -= soldierCost;
                Audio.Play("newUnit");
            }
            else
            {
                Audio.Play("click");
            }
        }

        public void BuyBaracks()
        {
            if (GameWorld.Gold >= Barracks.buildingPrice)
            {
                GameWorld.Gold -= Barracks.buildingPrice;
                Engine.I.AddGameObject(new Barracks());
                SetupMenu();
            }
        }

        public void BuyWorkerHousing()
        {
            if (GameWorld.Gold >= WorkerHousing.buildingPrice)
            {
                GameWorld.Gold -= WorkerHousing.buildingPrice;
                Engine.I.AddGameObject(new WorkerHousing());
                SetupMenu();
            }
        }

        public void BuyTradeSchool()
        {
            if (GameWorld.Gold >= TradeSchool.buildingPrice)
            {
                GameWorld.Gold -= TradeSchool.buildingPrice;
                Engine.I.AddGameObject(new TradeSchool());
                SetupMenu();
            }
        }

        public void BuyTreatStorage()
        {
            if (GameWorld.Gold >= TreatStorage.buildingPrice)
            {
                GameWorld.Gold -= TreatStorage.buildingPrice;
                Engine.I.AddGameObject(new TreatStorage());
                SetupMenu();
            }
        }

        public override void UpgradeBuilding()
        {
            if (GameWorld.Gold >= UpgradeGoldCost && GameWorld.Treats >= UpgradeTreatsCost)
            {
                GameWorld.Gold -= UpgradeGoldCost;
                GameWorld.Treats -= UpgradeTreatsCost;
                maxHealth *= 1.2f;
                health = maxHealth;
                Audio.Play("upgradeSound");
                buildingLevel++;
                Sprite = AssetManager.GetImage("Townhall/townhall_" + buildingLevel + ".png");
                SetupMenu();
                if (BuildingLevel >= 4)
                {
                    UIWin.Show();
                }
            }
            else
            {
                Audio.Play("click");
            }
        }
        protected override void OnDepletedHP()
        {
            UIGameOver.Show();
        }

        protected override void Update()
        {
            base.Update();
            CheckIfGameCanNotBeWon();
        }

       private void CheckIfGameCanNotBeWon()
        {
            if (GameWorld.CurrentWorkerCount <= 0 && GameWorld.Treats < WorkerCost)
            {
                int gold = GameWorld.Gold;
                int treats = GameWorld.Treats;

                for (int i = BuildingLevel; i < 4; i++)
                {
                    gold -= UpgradeGoldCost * i;
                    treats -= UpgradeTreatsCost * i;

                    if (gold < 0 || treats < 0)
                    {
                        UIGameOver.Show();
                        break;
                    }
                }
            }
        }

    }
}
