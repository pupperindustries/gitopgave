﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    class Barracks : Building
    {
        public static int buildingPrice = 50;
        private static Barracks i;
        public static Barracks I { get { return i; } }

        public Barracks() : base(AssetManager.GetImage("Buildings/baracks.png"), new Vector(5, 5f), (Vector.One * 1.2f), 200, "Barracks")
        {
            i = this;
        }

        protected override void OnBuildingDesctruction()
        {
            i = null;
            base.OnBuildingDesctruction();
        }
    }
}

