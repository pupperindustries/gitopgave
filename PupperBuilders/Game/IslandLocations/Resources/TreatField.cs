﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    class TreatField : Resource
    {
        private static TreatField i;
        public static TreatField I { get { return i; } }

        public TreatField() : base(AssetManager.GetImage("food.png"), new Vector(-5.3f, 5f), new Vector(3, 3), 0, "Treat Field", new Vector(0, 1.8f))
        {
            i = this;
            localRessource = GatheringResource.Treats;
        }
        


    }
}
