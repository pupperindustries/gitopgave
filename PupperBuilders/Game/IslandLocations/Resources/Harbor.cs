﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    class Harbor : Resource
    {
        private static Harbor i;
        public static Harbor I { get { return i; } }

        public Harbor() : base(AssetManager.GetImage("harbor.png"), new Vector(7.5f, -5), new Vector(5, 5), 0, "Harbor", new Vector(-0.9f, -1.5f))
        {
            i = this;
            localRessource = GatheringResource.Gold;
            Engine.I.AddGameObject(new ShipMerrill());
        }

    }
}
