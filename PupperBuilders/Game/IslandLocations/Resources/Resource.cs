﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PupperBuilders
{
    /// <summary>
    /// Location for farming resources
    /// </summary>
    abstract class Resource : IslandLocation
    {
        protected GatheringResource localRessource;
        /// <summary>
        /// The resource gained from working on this resource
        /// </summary>
        public GatheringResource LocalResource { get { return localRessource; } }

        public Resource(Image sprite, Vector position, Vector size, float rotation, string name, Vector nameOffset) : base(sprite, position, size, rotation, name, nameOffset)
        {

        }
    }
}
