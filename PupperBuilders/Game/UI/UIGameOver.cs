﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Menu overlay for game over 
    /// </summary>
    class UIGameOver : UIImage
    {
        private static UIGameOver I;
        /// <summary>
        /// width of the game over bar
        /// </summary>
        private static float width = 250;
        /// <summary>
        /// Height of the game over bar
        /// </summary>
        private static float height = 300;
        public static float Height
        {
            get
            {
                return height;
            }
        }

        /// <summary>
        /// The text displayed
        /// </summary>
        private string gameOverText = "Oh no.\n\nIt seems the cats' attacks were too strong for the brave doggers to rebuild the Pupper Island.\n\nYou reached wave :w: in :t:\n\nTry again?";

        public static bool IsEnabled { get { return I.Enabled; } }

        UIText childText;

        public UIGameOver() : base(AssetManager.GetImage("UI/game_over_panel.png"), new Vector(width, height), Vector.Zero)
        {
            I = this;

            childText = new UIText(gameOverText, 14, new Vector(10, 10));
            childText.size = Size - new Vector(20, 0);

            UIButton retryButton = new UIButton(AssetManager.GetImage("UI/button.png"), new Vector(120, 50), Vector.Zero);
            retryButton.Position = new Vector(Size.x / 2 - retryButton.Size.x / 2, Size.y - retryButton.Size.y - 20);
            retryButton.TextElement.text = "Retry";
            retryButton.OnClick = Click_Retry;

            AddChild(childText);
            AddChild(retryButton);

            Hide();
        }

        private void Click_Retry()
        {
            GameWorld.ResetGame();
        }

        public override void Update()
        {
            Position = new Vector(Engine.Window.Width / 2 - Size.x / 2, Engine.Window.Height / 2 - Size.y / 2);

            base.Update();
        }
        /// <summary>
        /// Shows the menu
        /// </summary>
        public static void Show()
        {
            float minutes = (float)Math.Floor(GameWorld.PlayTime / 60f);
            float seconds = GameWorld.PlayTime % 60f;
            string time = (minutes > 0 ? minutes + "m" : "") + Math.Floor(seconds) + "s";

            I.childText.text = I.gameOverText.Replace(":w:", EnemyManager.LevelIndex.ToString()).Replace(":t:", time);
            Engine.TimeScale = 0;
            I.Enabled = true;
            UIResourcesPanel.Hide();
            UISideMenu.Hide();
            Audio.StopAllSounds();
            Audio.Play("lose", true);
        }
        /// <summary>
        /// Hides the menu
        /// </summary>
        public static void Hide()
        {
            Engine.TimeScale = 1;
            I.Enabled = false;
            UIResourcesPanel.Show();
            UISideMenu.Show();
        }
    }
}
