﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Some UI text
    /// </summary>
    class UIText : UI
    {
        public string text;

        public Vector size;

        public int fontSize;

        public FontFamily fontFamily;

        public Color color;

        public StringFormat format;

        public bool hasShadow;

        public UIText(string text, int fontSize, Vector position)
        {
            this.text = text;
            this.fontSize = fontSize;
            color = Color.Black;
            hasShadow = false;
            fontFamily = AssetManager.GetFontFamily("default.ttf");

            this.Position = position;
            this.format = new StringFormat();
        }

        public override void Update()
        {
            base.Update();
        }

        /// <summary>
        /// Draw the text.
        /// </summary>
        /// <param name="graphics">The graphics we are drawing with.</param>
        public override void Draw(Graphics graphics)
        {
            if (size != Vector.Zero)
            {
                if (hasShadow)
                {
                    graphics.DrawString(text, new Font(fontFamily, fontSize, FontStyle.Regular), new SolidBrush(Color.Black), new Rectangle((int)WorldPosition.x + 2, (int)WorldPosition.y + 2, (int)size.x, (int)size.y), format);
                }
                graphics.DrawString(text, new Font(fontFamily, fontSize, FontStyle.Regular), new SolidBrush(color), new Rectangle((int)WorldPosition.x, (int)WorldPosition.y, (int)size.x, (int)size.y), format);
            }
            else
            {
                if(hasShadow)
                {
                    graphics.DrawString(text, new Font(fontFamily, fontSize, FontStyle.Regular), new SolidBrush(Color.Black), WorldPosition.x + 2, WorldPosition.y + 2, format);
                }
                graphics.DrawString(text, new Font(fontFamily, fontSize, FontStyle.Regular), new SolidBrush(color), WorldPosition.x, WorldPosition.y, format);
            }
            base.Draw(graphics);
        }
    }
}
