﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PupperBuilders
{
    /// <summary>
    /// A UI Image
    /// </summary>
    class UIImage : UI
    {
        private Image image;
        /// <summary>
        /// The image to display
        /// </summary>
        public Image Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        private Vector size;
        /// <summary>
        /// The size of the image in pixels
        /// </summary>
        public Vector Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        private float rotation;
        /// <summary>
        /// The way this image is rotated on the UI
        /// </summary>
        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
            }
        }

        public UIImage(Image image, Vector size, Vector position)
        {
            this.Position = position;
            this.Image = image;
            this.Size = size;
        }

        public override void Update()
        {
            base.Update();
        }

        public override void Draw(Graphics graphics)
        {
            if (Size.x < 1 || Size.y < 1)
                return; //We don't draw small images
            if (Image == null)
                return;

            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            //Find the center of the image.
            Vector pivot = WorldPosition + Size * new Vector(0.5f, 0.5f);

            //Change the origin of the coordinate to the center of the image.
            graphics.TranslateTransform(pivot.x, pivot.y);

            //Apply the rotation to the transformation matrix.
            graphics.RotateTransform(Rotation);

            //Reset the origin of the coordinate.
            graphics.TranslateTransform(-pivot.x, -pivot.y);

            //Draw the image.
            graphics.DrawImage(Image, WorldPosition.x, WorldPosition.y, Size.x, Size.y);

            //Reset the transformation matrix.
            graphics.ResetTransform();

            base.Draw(graphics);
        }
    }
}
