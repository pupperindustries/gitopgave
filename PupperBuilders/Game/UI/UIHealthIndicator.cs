﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Health indicator anything IDamagable
    /// </summary>
    class UIHealthIndicator : UIProgressBar
    {
        /// <summary>
        /// The unit to follow
        /// </summary>
        private IDamagable target;
        /// <summary>
        /// The amount of pixels above the target's center to display this text
        /// </summary>
        private float offset;

        public UIHealthIndicator(IDamagable target, int size, float verticalOffset, bool enemy) : base(AssetManager.GetImage("UI/health_bar.png"), AssetManager.GetImage("UI/health_bar_fill_player.png"), Vector.Zero, new Vector(40, 8))
        {
            if (enemy)
            {
                fillImage.Image = AssetManager.GetImage("UI/health_bar_fill_enemy.png");
            }
            this.target = target;
            this.offset = verticalOffset;
            Layer = UILayer.World;
        }

        public override void Update()
        {
            Position = Camera.WorldPosToScreen(target.GetPosition()) + (Vector.Up * offset);
            FillAmount = (float)target.GetCurrentHealth() / target.GetMaxHealth();

            base.Update();
        }
    }
}
