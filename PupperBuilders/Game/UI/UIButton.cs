﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// A UI Button
    /// </summary>
    class UIButton : UI
    {
        private Image image;
        /// <summary>
        /// The image of this button
        /// </summary>
        public Image Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        private Vector size;
        /// <summary>
        /// The size of this button
        /// </summary>
        public Vector Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        private UIText textElement;
        /// <summary>
        /// The text element to render on this 
        /// </summary>
        public UIText TextElement
        {
            get
            {
                return textElement;
            }
        }

        private bool lastFrameHovered;
        private Action onClick;
        /// <summary>
        /// The action to perform when this button is clicked
        /// </summary>
        public Action OnClick
        {
            get
            {
                return onClick;
            }
            set
            {
                onClick = value;
            }
        }


        public UIButton(Image image, Vector size, Vector position)
        {
            this.image = image;
            this.size = size;
            this.Position = position;
            this.textElement = new UIText("", 16, position);

            this.textElement.format.Alignment = StringAlignment.Center;
            this.textElement.format.LineAlignment = StringAlignment.Center;
            
            this.lastFrameHovered = false;

            AddChild(textElement);
        }

        public override void Draw(Graphics graphics)
        {
            if (Enabled)
            {
                Vector center = WorldPosition + size * 0.5f;
                Vector scaledPos = center - (size * 0.5f);
                graphics.DrawImage(image, scaledPos.x, scaledPos.y, (int)(size.x), (int)(size.y));

                base.Draw(graphics);
            }
        }

        public override void Update()
        {
            if (!Enabled || !AreParentsEnabled())
                return;

            textElement.size = size;

            Vector mousePosition = Input.MousePosition;
            Rectangle buttonRectangle = new Rectangle((int)WorldPosition.x, (int)WorldPosition.y, (int)(size.x), (int)(size.y));
            bool hovered = buttonRectangle.Contains((int)mousePosition.x, (int)mousePosition.y);

            if (hovered)
            {

                if (Input.MouseDown())
                {
                    onClick?.Invoke();
                }
            }

            lastFrameHovered = hovered;

            base.Update();
        }
    }
}
