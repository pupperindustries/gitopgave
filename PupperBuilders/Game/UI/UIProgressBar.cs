﻿//Lavet af Levi
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    class UIProgressBar : UI
    {
        /// <summary>
        /// The backgronud image of this progress bar
        /// </summary>
        protected UIImage backgroundImage;

        /// <summary>
        /// The image which fills based on fillAmount
        /// </summary>
        protected UIImage fillImage;

        /// <summary>
        /// The text in the middle of the progress bar
        /// </summary>
        protected UIText fillText;

        /// <summary>
        /// A float from 0 to 1 indicating how filled this progress bar is
        /// </summary>
        private float fillAmount;

        /// <summary>
        /// The size of the progress bar
        /// </summary>
        public Vector size;

        /// <summary>
        /// Sets or gets fillamount, updates fillImage to match the new fillAmount.
        /// </summary>
        public float FillAmount
        {
            set
            {
                fillAmount = value;
                fillAmount = Math.Min(1, fillAmount);
                fillAmount = Math.Max(0, fillAmount);
                fillImage.Size = new Vector(size.x / 1f * fillAmount, fillImage.Size.y);
            }
            get
            {
                return fillAmount;
            }
        }

        public UIProgressBar(Image backgroundImage, Image fillImage, Vector position, Vector size)
        {
            this.backgroundImage = new UIImage(backgroundImage, size + Vector.One * 2, Vector.Zero);
            this.backgroundImage.Position = new Vector(-this.backgroundImage.Size.x / 2,-1);

            this.fillImage = new UIImage(fillImage, size, Vector.Zero);
            this.fillImage.Position = new Vector(-this.fillImage.Size.x / 2, 0);

            this.fillText = new UIText("", 12, Vector.Zero);
            this.fillText.format.Alignment = StringAlignment.Center;
            this.fillText.format.LineAlignment = StringAlignment.Center;
            this.fillText.size = size;
            this.fillText.Position = new Vector(-this.fillText.size.x / 2, 0);

            this.FillAmount = fillAmount;
            this.Position = position;
            this.size = size;

            AddChild(this.backgroundImage);
            AddChild(this.fillImage);
            AddChild(this.fillText);
        }
    }
}
