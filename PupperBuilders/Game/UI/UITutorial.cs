﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// The tutorial menu
    /// </summary>
    class UITutorial : UIImage
    {
        private static UITutorial I;

        private string[] texts = new string[]
        {
            "After taking back their home island from the Fat Cat and their army of cat pirates, Captain Woofers and his friends start rebuilding the Pupper Island. To properly take back the island you need to upgrade your town hall all the way to level 4! The island has plenty of treats to be farmed and a harbor for trading to help build new buildings and recruit new soldiers and workers to help you rebuild. But the danger is not yet over! Cat pirates are still on the loose, trying to take back the island by sending in raids meant to halt your progress! Worry not, you have lots of good boyes at your disposal who are happy to help.",
            "How to play:\n"+
            "First click on one of your units and then click on a destination to assign your selected unit to it.\n"+
            "Workers can farm treats at the treat farm or trade goods to acquire gold at the harbor. Soldiers can patrol an area to defend it from attacking cats. \n"+
            "You can use your treats to buy more units, where gold can be used to buy buildings and upgrades to your units. Your goal is to upgrade the town hall to level 4 using treats and gold. \n"+
            "But be careful, if the hostile cats take down your town hall, it's game over!",
            "Made by:\n"+
            "Levi Moore\n"+
            "Camilla Vallentin\n"+
            "Daniel S. Å. Hansen\n\n"+
            "Music from Newgrounds:\n"+
            "Arend Van Stenis\n"+
            "Kyleandry\n"
        };
        private int index = -1;

        private List<UIText> uiTexts;
        private static float width = 400;
        private static float height = 350;

        private UIButton nextButton, preButton, doneButton;

        public static bool IsEnabled { get { return I.Enabled; } }

        public UITutorial() : base(AssetManager.GetImage("UI/tutorial_panel.png"), new Vector(width, height), Vector.Zero)
        {
            I = this;

            uiTexts = new List<UIText>();
            for (int i = 0; i < texts.Length; i++)
            {
                UIText uiText = new UIText(texts[i], 12, new Vector(15, 10));
                uiText.size = Size - new Vector(30, 0);
                uiTexts.Add(uiText);
                uiText.Enabled = false;
                AddChild(uiText);
            }

            nextButton = new UIButton(AssetManager.GetImage("UI/button.png"), new Vector(100, 40), Vector.Zero);
            nextButton.Position = new Vector(Size.x - nextButton.Size.x - 20, Size.y - nextButton.Size.y - 20);
            nextButton.TextElement.text = "Next";
            nextButton.OnClick = Next;

            preButton = new UIButton(AssetManager.GetImage("UI/button.png"), new Vector(100, 40), Vector.Zero);
            preButton.Position = new Vector(20, Size.y - nextButton.Size.y - 20);
            preButton.TextElement.text = "Previous";
            preButton.OnClick = Pre;

            doneButton = new UIButton(AssetManager.GetImage("UI/button.png"), new Vector(100, 40), Vector.Zero);
            doneButton.Position = new Vector(Size.x / 2 - doneButton.Size.x / 2, Size.y - doneButton.Size.y - 20);
            doneButton.TextElement.text = "Done";
            doneButton.OnClick = Hide;

            AddChild(nextButton);
            AddChild(preButton);
            AddChild(doneButton);

            Hide();
            Next();
        }

        public override void Update()
        {
            Position = new Vector(Engine.Window.Width / 2 - Size.x / 2, Engine.Window.Height / 2 - Size.y / 2);

            base.Update();
        }

        public void Next()
        {
            index++;
            if (index == texts.Length)
                index = texts.Length - 1;

            if (index > 0)
                uiTexts[index - 1].Enabled = false;

            ShowHideButtons();

            uiTexts[index].Enabled = true;
        }

        public void Pre()
        {
            index--;
            if (index < 0)
            {
                index = 0;
                nextButton.Enabled = true;
            }
            else
            {
                nextButton.Enabled = false;
            }

            if (index < uiTexts.Count - 1)
                uiTexts[index + 1].Enabled = false;

            ShowHideButtons();

            uiTexts[index].Enabled = true;
        }

        private void ShowHideButtons()
        {
            if (index == 0)
                preButton.Enabled = false;
            else
                preButton.Enabled = true;

            if (index == texts.Length - 1)
                nextButton.Enabled = false;
            else
                nextButton.Enabled = true;
        }

        public static void Show()
        {
            Engine.TimeScale = 0;
            I.Enabled = true;
            UISideMenu.Hide();
            UIResourcesPanel.Hide();
        }

        public static void Hide()
        {
            Engine.TimeScale = 1;
            I.Enabled = false;
            UISideMenu.Show();
            UIResourcesPanel.Show();
        }
    }
}
