﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    class UIIslandLocationName : UIText
    {
        private GameObject owner;
        private Vector nameOffset;

        public UIIslandLocationName(GameObject owner, string text, Vector nameOffset) : base(text, 12, Vector.Zero)
        {
            this.owner = owner;
            this.nameOffset = nameOffset;
            Layer = UILayer.World;
            format.Alignment = System.Drawing.StringAlignment.Center;
            format.LineAlignment = System.Drawing.StringAlignment.Center;
            color = System.Drawing.Color.White;
            hasShadow = true;
        }

        public override void Update()
        {
            Position = Camera.WorldPosToScreen(owner.Position + nameOffset);
            base.Update();
        }
    }
}
