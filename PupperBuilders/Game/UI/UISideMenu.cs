﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PupperBuilders
{
    /// <summary>
    /// The side menu
    /// </summary>
    class UISideMenu : UIImage
    {
        private static UISideMenu I;

        private float width = 180;
        public static float Width
        {
            get
            {
                return I.width;
            }
        }

        private UIImage topPanelImage, botPanelImage;

        public UISideMenu() : base(AssetManager.GetImage("UI/panel_mid.png"), Vector.Zero, Vector.Zero)
        {
            I = this;

            topPanelImage = new UIImage(AssetManager.GetImage("UI/panel_top.png"), new Vector(width, 20), Vector.Zero);
            botPanelImage = new UIImage(AssetManager.GetImage("UI/panel_bot.png"), new Vector(width, 20), Vector.Zero);
            AddChild(topPanelImage);
            AddChild(botPanelImage);
        }
        
        public override void Update()
        {
            Size = new Vector(width, Engine.Window.Size.Height);
            Position = new Vector(0, 0);
            botPanelImage.Position = new Vector(0, Size.y - botPanelImage.Size.y);

            base.Update();
        }
        /// <summary>
        /// Sets up the side panel to contain the specified buttons
        /// </summary>
        public static void SetPanelData( List<UISideMenuButton> buttons)
        {
            float buttonWidth = 140;
            float buttonHeight = 50;
            float marginToOtherButton = 10;
            float marginToTop = 20;


            foreach (var item in I.Children)
            {
                if(item is UIButton)
                    I.RemoveChild(item);
            }

            float positionY = marginToTop;
            foreach (var item in buttons)
            {
                UIButton button = new UIButton(AssetManager.GetImage("UI/button.png"), new Vector(buttonWidth, buttonHeight), Vector.Zero);
                button.Position = new Vector((I.width - button.Size.x) / 2, positionY);
                button.TextElement.text = item.ButtonText;
                button.OnClick += item.OnClick;
                button.TextElement.fontSize = item.FontSize;
                I.AddChild(button);

                positionY += button.Size.y + marginToOtherButton;
            }
        }

        public static void Show()
        {
            I.Enabled = true;
        }
        public static void Hide()
        {
            I.Enabled = false;
        }
    }
}
