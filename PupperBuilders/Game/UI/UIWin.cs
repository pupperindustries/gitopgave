﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// The menu for when winning
    /// </summary>
    class UIWin : UIImage
    {
        private static UIWin I;

        private static float width = 250;
        private static float height = 300;
        public static float Height
        {
            get
            {
                return height;
            }
        }

        private string winText = "Thanks to your help, Captain Woofers and the gang was able to rebuild the town hall!\n\nIf you would like, you can keep playing with the puppers and see how long you can keep going!";


        public static bool IsEnabled { get { return I.Enabled; } }

        public UIWin() : base(AssetManager.GetImage("UI/game_over_panel.png"), new Vector(width, height), Vector.Zero)
        {
            I = this;

            UIText text = new UIText(winText, 14, new Vector(10, 10));
            text.size = Size - new Vector(20, 0);

            UIButton continueButton = new UIButton(AssetManager.GetImage("UI/button.png"), new Vector(120, 50), Vector.Zero);
            continueButton.Position = new Vector(Size.x / 2 - continueButton.Size.x / 2, Size.y - continueButton.Size.y - 20);
            continueButton.TextElement.text = "Continue";
            continueButton.OnClick = Click_Continue;

            AddChild(text);
            AddChild(continueButton);

            Hide();
        }

        private void Click_Continue()
        {
            Hide();
        }

        public override void Update()
        {
            Position = new Vector(Engine.Window.Width / 2 - Size.x / 2, Engine.Window.Height / 2 - Size.y / 2);

            base.Update();
        }

        public static void Show()
        {
            Engine.TimeScale = 0;
            I.Enabled = true;
            UIResourcesPanel.Hide();
            UISideMenu.Hide();
        }
        public static void Hide()
        {
            Engine.TimeScale = 1;
            I.Enabled = false;
            UIResourcesPanel.Show();
            UISideMenu.Show();
        }
    }
}
