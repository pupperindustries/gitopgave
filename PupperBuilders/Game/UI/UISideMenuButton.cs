﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    class UISideMenuButton
    {
        private string buttonText;
        public string ButtonText
        {
            get
            {
                return buttonText;
            }
        }

        private Action onClick;
        public Action OnClick
        {
            get
            {
                return onClick;
            }
        }

        private int fontSize;
        public int FontSize
        {
            get
            {
                return fontSize;
            }
        }

        public UISideMenuButton(string buttonText, Action onClick, int textSize = 16)
        {
            this.buttonText = buttonText;
            this.onClick = onClick;
            this.fontSize = textSize;
        }
    }
}
