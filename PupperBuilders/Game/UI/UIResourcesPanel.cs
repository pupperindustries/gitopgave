﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// UI for showing all the different resources amassed
    /// </summary>
    class UIResourcesPanel : UIImage
    {
        private static UIResourcesPanel I;
        /// <summary>
        /// Width of the resource
        /// </summary>
        private static float width = 399;
        /// <summary>
        /// Height of the resource
        /// </summary>
        private static float height = 32;
        public static float Height
        {
            get
            {
                return height;
            }
        }

        private UIImage leftPanelImage, rightPanelImage;
        private UIText treatsText, goldText, soldiersText, workerText;

        public UIResourcesPanel() : base(AssetManager.GetImage("UI/panel_mid2.png"), new Vector(width, height), Vector.Zero)
        {
            I = this;

            leftPanelImage = new UIImage(AssetManager.GetImage("UI/panel_left.png"), new Vector(20, Size.y), Vector.Zero);
            rightPanelImage = new UIImage(AssetManager.GetImage("UI/panel_right.png"), new Vector(20, Size.y), new Vector(Size.x,0));
            treatsText = new UIText("", 12, new Vector(12, 6));
            goldText = new UIText("", 12, new Vector(120, 6));
            soldiersText = new UIText("", 12, new Vector(220, 6));
            workerText = new UIText("", 12, new Vector(310, 6));

            AddChild(leftPanelImage);
            AddChild(rightPanelImage);
            AddChild(treatsText);
            AddChild(goldText);
            AddChild(soldiersText);
            AddChild(workerText);
        }

        public override void Update()
        {
            Position = new Vector(UISideMenu.Width + 5, 0);
            treatsText.text = "Treats: " + GameWorld.Treats + "(+" + GameWorld.WorkerTreatCapacity + ")";
            goldText.text = "Gold: " + GameWorld.Gold + "(+" + GameWorld.WorkerGoldCapacity + ")";
            soldiersText.text = "Soldiers: " + GameWorld.CurrentSoldierCount + "/" + GameWorld.SoldierCapacity;
            workerText.text = "Workers: " + GameWorld.CurrentWorkerCount + "/" + GameWorld.WorkerCapacity;

            base.Update();
        }

        public static void Show()
        {
            I.Enabled = true;
        }
        public static void Hide()
        {
            I.Enabled = false;
        }
    }
}
