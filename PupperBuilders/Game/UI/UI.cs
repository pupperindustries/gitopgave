﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Parent class for all UI
    /// </summary>
    abstract class UI
    {
        private bool enabled = true;
        /// <summary>
        /// Whether or not this UI element is enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }
        private UI parent;
        /// <summary>
        /// The parent of this ui element
        /// </summary>
        public UI Parent
        {
            get
            {
                return parent;
            }
        }
        private List<UI> children = new List<UI>();
        /// <summary>
        /// Children elements of the ui element
        /// </summary>
        public List<UI> Children
        {
            get
            {
                return children;
            }
        }
        
        private List<UI> childrenToAdd = new List<UI>();
        private List<UI> childrenToRemove = new List<UI>();

        private Vector position;
        /// <summary>
        /// The local position of the ui element
        /// </summary>
        public Vector Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        private Vector worldPosition;
        /// <summary>
        /// The actual position this ui is rendered
        /// </summary>
        public Vector WorldPosition
        {
            get
            {
                return worldPosition;
            }
        }

        private UILayer layer = UILayer.Overlay;
        /// <summary>
        /// The layer this ui is rendered on. Used for sorting ui elements
        /// </summary>
        public UILayer Layer
        {
            get
            {
                return layer;
            }
            set
            {
                layer = value;
            }
        }
        /// <summary>
        /// Adds a child to this ui element
        /// </summary>
        public void AddChild(UI child)
        {
            lock (childrenToAdd)
            {
                if (!childrenToAdd.Contains(child))
                {
                    childrenToAdd.Add(child);
                }
            }
        }
        /// <summary>
        /// Removes the specified child from this ui's children
        /// </summary>
        public void RemoveChild(UI child)
        {
            lock (childrenToRemove)
            {
                if (!childrenToRemove.Contains(child))
                {
                    childrenToRemove.Add(child);
                }
            }
        }

        public virtual void Update()
        {
            if (parent == null)
                worldPosition = position;

            AddQueuedChildren();
            RemoveQueuedChildren();
            foreach (UI child in children)
            {
                if (child != null && child.enabled)
                {
                    child.worldPosition = worldPosition + child.position;
                    child.Update();
                }
            }
        }
        public virtual void Draw(Graphics graphics)
        {
            foreach (UI child in children)
            {
                if (child != null && child.enabled)
                {
                    child.Draw(graphics);
                }
            }
        }
        /// <summary>
        /// Returns true if no parent in this ui's lineage is disabled
        /// </summary>
        public bool AreParentsEnabled()
        {
            if (Parent == null)
            {
                return enabled;
            }
            else
            {
                return Parent.AreParentsEnabled();
            }
        }

        private void AddQueuedChildren()
        {
            lock (childrenToAdd)
            {
                foreach (UI ui in childrenToAdd)
                {
                    if (children.Contains(ui))
                        continue;

                    children.Add(ui);
                    ui.parent = this;
                }
                childrenToAdd.Clear();
            }
        }
        private void RemoveQueuedChildren()
        {
            lock (childrenToRemove)
            {
                foreach (UI ui in childrenToRemove)
                {
                    if (!children.Contains(ui))
                        continue;

                    children.Remove(ui);
                    ui.parent = null;
                }
                childrenToRemove.Clear();
            }
        }
    }
}
