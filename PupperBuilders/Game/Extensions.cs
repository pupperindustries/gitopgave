﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// This class contains definitions for various extension functions. 
    /// </summary>
    static class Extensions
    {
        /// <summary>
        /// Returns true if this number is smaller than 1e-10f and greater than -1e-10f
        /// </summary>
        public static bool IsZero(this float f)
        {
            return Math.Abs(f) < 1e-10f;
        }
        /// <summary>
        /// Linearly interpolate this float towards target based on fraction.
        /// </summary>
        public static float Lerp(this float f, float target, float fraction)
        {
            return (target - f) * fraction + f;
        }

        /// <summary>
        /// Same as Lerp but makes sure the values interpolate correctly when they wrap around 360 degrees.
        /// </summary>
        public static float LerpAngle(this float a, float b, float t)
        {
            float num = Repeat(b - a, 360f);
            if ((double)num > 180.0)
                num -= 360f;
            return a + num * Clamp01(t);
        }

        /// <summary>
        /// Loops the value t, so that it is never larger than length and never smaller than 0.
        /// </summary>
        public static float Repeat(float t, float length)
        {
            return t - (float)Math.Floor(t / length) * length;
        }

        /// <summary>
        /// Clamps value between 0 and 1 and returns value.
        /// </summary>
        public static float Clamp01(float value)
        {
            if ((double)value < 0.0)
                return 0.0f;
            if ((double)value > 1.0)
                return 1f;
            return value;
        }
    }
}
