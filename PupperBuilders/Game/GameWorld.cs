﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    static class GameWorld
    {
        public static readonly object resourceLock = new Object();

        /// <summary>
        /// The amount of treats the player has available
        /// </summary>
        private static int treats;
        public static int Treats { get { lock (resourceLock) return treats; } set { lock (resourceLock) treats = value; } }

        /// <summary>
        /// The amount of gold the player has available
        /// </summary>
        private static int gold;
        public static int Gold { get { lock (resourceLock) return gold; } set { lock (resourceLock) gold = value; } }

        /// <summary>
        /// The maximum amount of soldiers the player can recruit
        /// </summary>
        public static int SoldierCapacity
        {
            get
            {
                lock (resourceLock)
                {
                    int soldierCapacity = 3;
                    if (Barracks.I != null)
                        return soldierCapacity + Barracks.I.BuildingLevel;
                    return soldierCapacity;
                }
            }
        }

        /// <summary>
        /// The amount of soldiers the player currently have in the world
        /// </summary>
        public static int CurrentSoldierCount
        {
            get
            {
                lock (resourceLock)
                {
                    int currentSoldierCount = Engine.GameObjectsWhere(gameObject => gameObject is PlayerSoldier).Length;
                    return currentSoldierCount;
                }
            }
        }

        public static int WorkerCapacity
        {
            get
            {
                lock (resourceLock)
                {
                    int workerCapacity = 3;
                    if (WorkerHousing.I != null)
                        return workerCapacity + WorkerHousing.I.BuildingLevel;
                    return workerCapacity;
                }
            }
        }

        public static int CurrentWorkerCount
        {
            get
            {
                int workers = Engine.GameObjectsWhere(gameObject => gameObject is Worker).Length;
                return workers;
            }
        }

        public static int WorkerGoldCapacity
        {
            get
            {
                int gold = 5;
                if (TradeSchool.I != null)
                    return gold + (TradeSchool.I.BuildingLevel * 2);
                return gold;
            }
        }

        public static int WorkerTreatCapacity
        {
            get
            {
                int treat = 1;
                if (TreatStorage.I != null)
                    return treat + TreatStorage.I.BuildingLevel;
                return treat;
            }
        }

        /// <summary>
        /// How many seconds the game has been played
        /// </summary>
        public static float PlayTime = 0f;
        
        public static void AddToScene(Engine engine)
        {
            PlayTime = 0f;
            Audio.AssignNameToAudioFile("catDeath", "catDeath.mp3");
            Audio.AssignNameToAudioFile("catShot", "catShot.wav");
            Audio.AssignNameToAudioFile("dogDeath", "dogDeath.mp3");
            Audio.AssignNameToAudioFile("dogSelect", "dogSelect.wav");
            Audio.AssignNameToAudioFile("dogShot", "dogShot.wav");
            Audio.AssignNameToAudioFile("gainMoney", "gainMoney.mp3");
            Audio.AssignNameToAudioFile("upgradeSound", "upgradeSound.mp3");
            Audio.AssignNameToAudioFile("newUnit", "newUnit.mp3");
            Audio.AssignNameToAudioFile("click", "click.mp3");
            Audio.AssignNameToAudioFile("thud2", "thud2.wav");
            Audio.AssignNameToAudioFile("woodBreak2", "woodBreak2.wav");
            Audio.AssignNameToAudioFile("bgm", "Music/bgm.mp3");
            Audio.AssignNameToAudioFile("lose", "Music/lose.mp3");


            engine.AddGameObject(new Water());
            engine.AddGameObject(new Island());
            Camera.Height = Island.IslandSize + 2;

            engine.AddUI(new UISideMenu());
            engine.AddUI(new UIResourcesPanel());
            engine.AddUI(new UIGameOver());
            engine.AddUI(new UIWin());
            engine.AddUI(new UITutorial());
            engine.AddGameObject(new MainBuilding());
            engine.AddGameObject(new Harbor());
            engine.AddGameObject(new TreatField());
            engine.AddGameObject(new SelectorObject());
            engine.AddGameObject(new Worker(Rng.OnUnitCircle * 1.5f));
            engine.AddGameObject(new PlayerSoldier(Vector.Zero));

            treats = 0;
            gold = 0;
            Audio.StopAllSounds();
            Audio.Play("bgm", true, 0.2f);

            EnemyManager.Start();
            UITutorial.Show();
        }

        public static void ResetGame()
        {
            Engine.I.ClearScene();
            AddToScene(Engine.I);
        }
    }
}
