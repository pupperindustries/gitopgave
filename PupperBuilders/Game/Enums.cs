﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    enum Side
    {
        Left,
        Right,
        Up,
        Down
    }

    enum GatheringResource { Gold, Treats, None }
    enum GameObjectLayer { Background, Foreground, Unit, Indicator }
    enum UILayer { World, Overlay}
}
