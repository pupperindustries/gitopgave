﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
namespace PupperBuilders
{
    /// <summary>
    /// A gameobject
    /// </summary>
    abstract class GameObject
    {
        public bool LoopRunning = false;
        public bool Running = false;

        /// <summary>
        /// AutoResetEvent for running a single cycle
        /// </summary>
        public AutoResetEvent RunEvent { get; private set; }

        /// <summary>
        /// The thread this game object's game loop runs on.
        /// </summary>
        private Thread gameLoopThread;

        /// <summary>
        /// Lock for sprite
        /// </summary>
        private readonly object spriteLock = new object();
        private Image sprite;
        /// <summary>
        /// The image of this gameobject
        /// </summary>
        protected Image Sprite
        {
            get
            {
                lock (spriteLock)
                {
                    return sprite;
                }
            }
            set
            {
                lock (spriteLock)
                {
                    sprite = value;
                }
            }
        }

        private readonly object positionLock = new object();
        private Vector position;
        /// <summary>
        /// Position in the gameworld of this gameobject
        /// </summary>
        public Vector Position
        {
            get
            {
                lock (positionLock)
                {
                    return position;
                }
            }
            set
            {
                lock (positionLock)
                {
                    position = value;
                }
            }
        }

        private readonly object sizeLock = new object();
        private Vector size;
        /// <summary>
        /// Size in game units of this gameobject
        /// </summary>
        protected Vector Size
        {
            get
            {
                lock (sizeLock)
                {
                    return size;
                }
            }
            set
            {
                lock (sizeLock)
                {
                    size = value;
                }
            }
        }

        private readonly object rotationLock = new object();
        private float rotation;
        /// <summary>
        /// Rotation of this gameobject in degrees
        /// </summary>
        protected float Rotation
        {
            get
            {
                lock (rotationLock)
                {
                    return rotation;
                }
            }
            set
            {
                lock (rotationLock)
                {
                    rotation = value;
                }
            }
        }

        protected GameObjectLayer layer;
        /// <summary>
        /// The layer this gameobject is on. Used for sorting rendering.
        /// </summary>
        public GameObjectLayer Layer
        {
            get
            {
                return layer;
            }
        }

        public GameObject(Image sprite, Vector position, Vector size, float rotation)
        {
            this.Sprite = sprite;
            this.position = position;
            this.Size = size;
            this.Rotation = rotation;
            layer = GameObjectLayer.Foreground;
        }

        /// <summary>
        /// Starts the gameobject, opening its game loop thread
        /// </summary>
        public void StartThread()
        {
            if (Running)
                return;

            RunEvent = new AutoResetEvent(false);
            Running = true;
            gameLoopThread = new Thread(GameLoop);
            gameLoopThread.IsBackground = true;
            gameLoopThread.Start();
        }

        /// <summary>
        /// Closes the thread for this GameObject
        /// </summary>
        public void Close()
        {
            Running = false;
            gameLoopThread.Abort();
        }

        /// <summary>
        /// Entry point for this game object's game loop thread
        /// </summary>
        private void GameLoop()
        {
            Start();

            while (Running)
            {
                //First, wait for our RunEvent to be raised - getting to "go" signal from the main game loop thread
                RunEvent.WaitOne();

                //If running was set to false while waiting for the run event, just break
                if (!Running)
                {
                    break;
                }
                Update();

                //When loop running is false, the main game loop thread knows I'm done and am waiting for the run event again
                LoopRunning = false;
            }
        }

        public void Draw(Graphics gfx)
        {
            lock (Engine.I.graphicsLock)
            {
                if (Engine.FormWindowState == FormWindowState.Minimized)
                    return;
                if (Sprite == null)
                    return;

                //Get the width, height and position from the Transform class.
                Vector size = Camera.WorldSizeToScreen(this.Size);
                Vector screenPosition = Camera.WorldPosToScreen(this.position - this.Size * 0.5f);

                //Find the center of the image.
                Vector center = screenPosition + size * 0.5f;

                //Change the origin of the coordinate to the center of the image.
                gfx.TranslateTransform(center.x, center.y);

                //Apply the rotation to the transformation matrix.
                gfx.RotateTransform(this.Rotation);

                //Reset the origin of the coordinate.
                gfx.TranslateTransform(-center.x, -center.y);

                //Draw the image.
                gfx.DrawImage(Sprite, screenPosition.x, screenPosition.y, size.x, size.y);

                //Reset the transformation matrix.
                gfx.ResetTransform();
            };
        }
        /// <summary>
        /// Returns true if this gameobject was clicked this frame
        /// </summary>
        protected bool IsClicked()
        {
            Vector mousePosition = Camera.ScreenPosToWorld(Input.MousePosition);
            if ((mousePosition - Position).Length < Math.Max(Size.x, Size.y) && Input.MouseDown())
                return true;
            else
                return false;
        }

        /// <summary>
        /// Changes either the width or height of this object to match the sprite's original aspect ratio
        /// </summary>
        protected void MatchAspectRatio(bool keepWidth)
        {
            if(keepWidth)
            {
                size = new Vector(size.x, size.x * ((float)Sprite.Height / Sprite.Width));
            }
            else
            {
                size = new Vector(size.y * ((float)Sprite.Width / Sprite.Height), size.y);
            }
        }

        protected virtual void Start() { }
        protected virtual void Update() { }
        public virtual void OnRemove() { }
    }
}
