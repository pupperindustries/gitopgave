﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// A bullet
    /// </summary>
    class Bullet : GameObject
    {
        /// <summary>
        /// the target of this bullet
        /// </summary>
        private IDamagable target;
        /// <summary>
        /// How fast this bullet moves
        /// </summary>
        const float speed = 5f;
        /// <summary>
        /// The damage of this bullet
        /// </summary>
        private float damage = 10f;
        public Bullet(Vector pos, IDamagable target, float damage) : base(AssetManager.GetImage("bullet.png"), pos, Vector.One * 0.1f, 0f)
        {
            this.target = target;
            this.damage = damage;
        }
        protected override void Update()
        {
            Vector targetVector = target.GetPosition() - Position;
            float frameDistance = speed * Engine.DeltaTime;
            Position += targetVector.Normalized * frameDistance;
            if (frameDistance >= targetVector.Length)
            {
                HitUnit();
            }
            base.Update();
        }
        private void HitUnit()
        {
            target.TakeDamage(damage);
            Engine.I.RemoveGameObject(this);
        }
    }
}
