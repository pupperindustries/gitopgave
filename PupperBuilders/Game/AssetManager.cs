﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IrrKlang;

namespace PupperBuilders
{
    /// <summary>
    /// Class to load assets
    /// </summary>
    static class AssetManager
    {
        /// <summary>
        /// Local path to folder containing all the image files.
        /// </summary>
        private static string localImageFolder = "Assets/Images/";

        /// <summary>
        /// Local path to folder containing all the audio files.
        /// </summary>
        private static string localAudioFolder = "Assets/Audio/";

        /// <summary>
        /// Local path to folder containing all font files.
        /// </summary>
        private static string localFontFolder = "Assets/Fonts/";

        /// <summary>
        /// Sound engine used to load audio file with.
        /// </summary>
        private static ISoundEngine soundEngine = new ISoundEngine();

        /// <summary>
        /// List of images that has already been loaded.
        /// </summary>
        private static Dictionary<string, Image> loadedImages = new Dictionary<string, Image>();

        /// <summary>
        /// list of audio that has already been loaded.
        /// </summary>
        private static Dictionary<string, ISoundSource> loadedAudio = new Dictionary<string, ISoundSource>();

        /// <summary>
        /// list of fonts that has already been loaded.
        /// </summary>
        private static Dictionary<string, PrivateFontCollection> loadedFont = new Dictionary<string, PrivateFontCollection>();

        /// <summary>
        /// Get a image from the assets folder
        /// </summary>
        /// <param name="name">Name of the image you want.</param>
        /// <returns></returns>
        public static Image GetImage(string name)
        {
            lock (loadedImages)
            {
                //If the image has already been loaded once. Return the loaded image instead of loading it again.
                if (loadedImages.ContainsKey(name))
                {
                    return loadedImages[name];
                }
                else
                {
                    //If the image file exists on the disk. Add it to the list of loaded images and return it.
                    if (File.Exists(localImageFolder + name))
                    {
                        loadedImages.Add(name, Image.FromFile(localImageFolder + name));
                        return loadedImages[name];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Get audio from the assets folder.
        /// </summary>
        /// <param name="name">Name of the audio you want.</param>
        /// <returns></returns>
        public static ISoundSource GetAudio(string name)
        {
            lock (loadedAudio)
            {
                //If the audio has already been loaded once. Return the loaded audio instead of loading it again.
                if (loadedAudio.ContainsKey(name))
                {
                    return loadedAudio[name];
                }
                else
                {
                    //If the audio file exists on the disk. Add it to the list of loaded audio and return it.
                    if (File.Exists(localAudioFolder + name))
                    {
                        loadedAudio.Add(name, soundEngine.AddSoundSourceFromFile(localAudioFolder + name));
                        return loadedAudio[name];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Get font from the assets folder.
        /// </summary>
        /// <param name="name">Name of the font you want.</param>
        /// <returns></returns>
        public static FontFamily GetFontFamily(string name)
        {
            lock (loadedFont)
            {
                //If the font has already been loaded once. Return the loaded font instead of loading it again.
                if (loadedFont.ContainsKey(name))
                {
                    return loadedFont[name].Families[0];
                }
                else
                {
                    //If the font file exists on the disk. Add it to the list of loaded font and return it.
                    if (File.Exists(localFontFolder + name))
                    {
                        //We can not load a font that has not been installed on the device.
                        //We can get around this problem by adding it to a collection of private fonts by using the PrivateFontCollection class.
                        PrivateFontCollection pfc = new PrivateFontCollection();
                        pfc.AddFontFile(localFontFolder + name);
                        loadedFont.Add(name, pfc);
                        return loadedFont[name].Families[0];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
    }
}
