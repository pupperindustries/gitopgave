﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
namespace PupperBuilders
{
    /// <summary>
    /// Handles adding things to the game world and engine stuff
    /// </summary>
    class Engine
    {
        #region Fields
        public static Engine I { get; private set; }
        public bool useDebugWindow = true;

        //Thread - Gameloop
        /// <summary>
        /// The thread running the game loop
        /// </summary>
        private Thread gameLoopThread;

        /// <summary>
        /// While true, game loop thread runs game loops.
        /// </summary>
        public bool Running = false;
        
        //Windows
        /// <summary>
        /// The form
        /// </summary>
        private Form form;

        /// <summary>
        /// The current window rectangle
        /// </summary>
        private Rectangle window;
        public static Rectangle Window
        {
            get
            {
                return I.window;
            }
        }

        public static FormWindowState FormWindowState { get { return I.form.WindowState; } }

        //Graphics
        /// <summary>
        /// Graphics device used for drawing
        /// </summary>
        private Graphics graphics;

        /// <summary>
        /// Backbuffer for the graphics device
        /// </summary>
        private BufferedGraphics backBuffer;

        /// <summary>
        /// Used for locking graphics
        /// </summary>
        public readonly object graphicsLock = new object();
        /// <summary>
        /// Lock used for time related stuff
        /// </summary>
        public readonly object timeLock = new object();
        //Time
        /// <summary>
        /// Envinroment.TickCount on the last frame
        /// </summary>
        private int lastTickCount;

        /// <summary>
        /// Time in seconds since last frame
        /// </summary>
        private float deltaTime;
        /// <summary>
        /// Time in seconds since last frame
        /// </summary>
        public static float DeltaTime
        {
            get
            {
                lock (I.timeLock)
                {
                    return I.deltaTime;
                }
            }
        }

        /// <summary>
        /// Time in seconds since last frame
        /// </summary>
        private float realDeltaTime;
        /// <summary>
        /// Time in seconds since last frame
        /// </summary>
        public static float RealDeltaTime
        {
            get
            {
                lock (I.timeLock)
                {
                    return I.realDeltaTime;
                }
            }
        }

        /// <summary>
        /// The time in seconds since the game started
        /// </summary>
        private float totalTime;
        /// <summary>
        /// The time in seconds since the game started
        /// </summary>
        public static float TotalTime
        {
            get
            {
                lock(I.timeLock)
                    return I.totalTime;
            }
        }

        /// <summary>
        /// The time in seconds since the game started
        /// </summary>
        private float realTotalTime;
        /// <summary>
        /// The time in seconds since the game started
        /// </summary>
        public static float RealTotalTime
        {
            get
            {
                lock(I.timeLock) return I.realTotalTime;
            }
        }

        /// <summary>
        /// The speed the game is running with.
        /// </summary>
        private float timeScale;
        /// <summary>
        /// The speed the game is running with.
        /// </summary>
        public static float TimeScale
        {
            get
            {
                lock(I.timeLock) return I.timeScale;
            }
            set
            {
                lock (I.timeLock)
                {
                    value = Math.Max(0, value);
                    I.timeScale = value;
                }
            }
        }

        //GameObjects
        /// <summary>
        /// GameObjects currently in the scene
        /// </summary>
        private List<GameObject> gameObjects;
        /// <summary>
        /// GameObjects to add to the scene
        /// </summary>
        private List<GameObject> gameObjectsToAdd;
        /// <summary>
        /// GameObjects to remove from the scene
        /// </summary>
        private List<GameObject> gameObjectToRemove;

        //UI
        /// <summary>
        /// GameObjects currently in the scene
        /// </summary>
        private List<UI> uiElements;
        /// <summary>
        /// GameObjects to add to the scene
        /// </summary>
        private List<UI> uiElementsToAdd;
        /// <summary>
        /// GameObjects to remove from the scene
        /// </summary>
        private List<UI> uiElementsToRemove;

        /// <summary>
        /// True, if the game is paused
        /// </summary>
        private bool isGamePaused = false;

        #endregion

        #region Methods

        /// <summary>
        /// Instantiates a new game world
        /// </summary>
        public Engine(Form form, Graphics graphics, Rectangle displayRect)
        {
            I = this;
            timeScale = 1f;
            gameObjects = new List<GameObject>();
            gameObjectsToAdd = new List<GameObject>();
            gameObjectToRemove = new List<GameObject>();
            uiElements = new List<UI>();
            uiElementsToAdd = new List<UI>();
            uiElementsToRemove = new List<UI>();
            lastTickCount = int.MinValue;
            window = displayRect;
            this.form = form;
            SetGraphics(graphics, window);
            Input.Form = form;
            Input.SetupInput();
            StartGame();
        }

        /// <summary>
        /// Sets a new graphics device and screen size.
        /// </summary>
        public void SetGraphics(Graphics gfx, Rectangle rect)
        {
            lock (graphicsLock)
            {
                this.backBuffer = BufferedGraphicsManager.Current.Allocate(gfx, rect);
                graphics = backBuffer.Graphics;
                this.window = rect;
                rect.X = form.Left;
                rect.Y = form.Top;
            }
        }

        /// <summary>
        /// Starts the game loop thread, if it's not already running
        /// </summary>
        public void StartGame()
        {
            if (Running)
                return;

            if (useDebugWindow)
                Debug.Start();
            GameWorld.AddToScene(this);
            gameLoopThread = new Thread(GameLoop);
            gameLoopThread.IsBackground = true;
            Running = true;
            gameLoopThread.Start();
        }
        public void CloseEngine()
        {
            lock (gameObjects)
            {
                foreach (GameObject gameObject in gameObjects)
                {
                    gameObject.Close();
                }
            }
            gameLoopThread.Abort();
            EnemyManager.Abort();
        }
        
        //Gameloop
        /// <summary>
        /// Entry point for the game loop thread. 
        /// </summary>
        private void GameLoop()
        {
            while (Running)
            {
                bool deltaTimeResult = UpdateDeltaTime();
                if (!deltaTimeResult)
                    continue;

                Input.FrameUpdate();

                PauseUpdate();

                RemoveQueuedGameObjects();
                AddQueuedGameObjects();
                GameObjectsUpdate();

                AddQueuedUIElements();
                RemoveQueuedUIElements();

                foreach (UI ui in uiElements)
                {
                    ui.Update();
                }
                Draw();
            }
        }

        private void PauseUpdate()
        {
            if(UIGameOver.IsEnabled || UIWin.IsEnabled || UITutorial.IsEnabled)
            {
                isGamePaused = false;
                return;
            }
            if(Input.KeyDown(Keys.Escape))
            {
                isGamePaused = !isGamePaused;
                timeScale = isGamePaused ? 0 : 1;
            }
            GameWorld.PlayTime += DeltaTime;
        }

        /// <summary>
        /// Calls all draw calls
        /// </summary>
        private void Draw()
        {
            if (Engine.FormWindowState == FormWindowState.Minimized)
                return;

            lock (graphicsLock)
                graphics.Clear(Color.White);

            foreach (GameObject obj in gameObjects)
            {
                obj.Draw(graphics);
            }
            foreach (UI ui in uiElements)
            {
                if (ui.Enabled)
                    ui.Draw(graphics);
            }
            lock (graphicsLock)
            {
                //graphics.DrawString("Wave: " + EnemyManager.LevelIndex, new Font("Arial", 12), new SolidBrush(Color.Black), 2, window.Height - 20);
                if(isGamePaused)
                {
                    float minutes = (float)Math.Floor(GameWorld.PlayTime / 60f);
                    float seconds = GameWorld.PlayTime % 60f;
                    string time = (minutes > 0 ? minutes + "m" : "") + Math.Floor(seconds) + "s";
                    string text = "PAUSED - Wave: " + EnemyManager.LevelIndex + " - Time played: "+time;
                    float fontSize = 20;
                    float x = window.Width / 2 - 100;
                    float y = window.Height / 2 - 10;
                    graphics.DrawString(text, new Font(AssetManager.GetFontFamily("default.ttf"), 20, FontStyle.Regular), new SolidBrush(Color.Black), x + 2, y + 2);
                    graphics.DrawString(text, new Font(AssetManager.GetFontFamily("default.ttf"), 20, FontStyle.Regular), new SolidBrush(Color.White), x , y);
                }
                backBuffer.Render();
            }
        }

        /// <summary>
        /// Updates delta time. Returns true if more than 0ms has passed, and delta time was updated.
        /// </summary>
        /// <returns></returns>
        private bool UpdateDeltaTime()
        {
            int tickCount = Environment.TickCount;
            if (lastTickCount == int.MinValue)
                lastTickCount = Environment.TickCount;
            //To avoid having a frame with a deltatime of 0ms, we continue until at least 1 ms has passed
            if (tickCount - lastTickCount < 1)
            {
                return false;
            }
            realDeltaTime = (tickCount - lastTickCount) / 1000f;
            deltaTime = realDeltaTime * timeScale;
            totalTime += deltaTime;
            realTotalTime += realDeltaTime;
            lastTickCount = tickCount;
            return true;
        }

        //GameObjects
        /// <summary>
        /// Prepare the gameobject to be added next frame
        /// </summary>
        public void AddGameObject(GameObject obj)
        {
            lock (gameObjectsToAdd)
            {
                gameObjectsToAdd.Add(obj);
            }
        }
        /// <summary>
        /// Queue the specified object to be removed on the next frame.
        /// </summary>
        public void RemoveGameObject(GameObject obj)
        {
            lock (gameObjectToRemove)
            {
                gameObjectToRemove.Add(obj);
            }
        }
        /// <summary>
        /// Adds all game objects to the active game oject list, and starts their game loop threads
        /// </summary>
        private void AddQueuedGameObjects()
        {
            lock (gameObjects)
            {
                lock (gameObjectsToAdd)
                {
                    if (gameObjectsToAdd.Count < 1)
                        return;
                    foreach (GameObject obj in gameObjectsToAdd)
                    {
                        if (gameObjects.Contains(obj))
                            continue;
                        gameObjects.Add(obj);
                        obj.StartThread();
                    }
                    gameObjectsToAdd.Clear();
                    gameObjects = gameObjects.OrderBy(gameObject => (int)gameObject.Layer).ToList();
                }
            }
        }
        /// <summary>
        /// Remove all objects that have been queued to be removed and stopped.
        /// </summary>
        private void RemoveQueuedGameObjects()
        {
            lock (gameObjects)
            {
                lock (gameObjectToRemove)
                {
                    foreach (GameObject obj in gameObjectToRemove)
                    {
                        obj.Running = false;
                        obj.RunEvent.Set();
                        obj.OnRemove();
                        gameObjects.Remove(obj);
                    }
                    gameObjectToRemove.Clear();
                }
            }
        }

        /// <summary>
        /// Runs updates on all game object threads
        /// </summary>
        private void GameObjectsUpdate()
        {
            foreach (GameObject obj in gameObjects)
            {
                //By calling set on RunEvent, this object's update loop runs one cycle. By setting LoopRunning to true first, we ensure that it will only be false again, once the loop concludes.
                obj.LoopRunning = true;
                obj.RunEvent.Set();
            }
            bool allDone = false;
            //Do busy waiting until all game loop threads are done.
            while (!allDone)
            {
                //Assume all are done. 
                allDone = true;
                foreach (GameObject obj in gameObjects)
                {
                    if (obj.LoopRunning)
                    {
                        //When we find just one game object, which isn't done, we'll flip all done back
                        allDone = false;
                        break;
                    }
                }
            }
            //Now all loops are done, they'll be waiting for their next autoresetevent to be raised.
        }

        public static GameObject[] GameObjectsWhere(Func<GameObject, bool> func)
        {
            return I.gameObjects.Where(func).ToArray();
        }

        //UI
        public void AddUI(UI ui)
        {
            lock (uiElementsToAdd)
            {
                uiElementsToAdd.Add(ui);
            }
        }
        public void RemoveUI(UI ui)
        {
            lock (uiElementsToRemove)
            {
                uiElementsToRemove.Add(ui);
            }
        }
        private void AddQueuedUIElements()
        {
            lock (uiElementsToAdd)
            {
                foreach (UI ui in uiElementsToAdd)
                {
                    if (uiElements.Contains(ui))
                        continue;

                    uiElements.Add(ui);
                }
                uiElementsToAdd.Clear();
                uiElements = uiElements.OrderBy(uiElement => (int)uiElement.Layer).ToList();
            }
        }
        private void RemoveQueuedUIElements()
        {
            lock (uiElementsToRemove)
            {
                foreach (UI ui in uiElementsToRemove)
                {
                    if (!uiElements.Contains(ui))
                        continue;

                    uiElements.Remove(ui);
                }
                uiElementsToRemove.Clear();
            }
        }
        /// <summary>
        /// Removes all gameobjects and UI
        /// </summary>
        public void ClearScene()
        {
            lock(uiElementsToRemove)
            {
                uiElementsToRemove.AddRange(uiElements);
            }
            lock(gameObjectToRemove)
            {
                gameObjectToRemove.AddRange(gameObjects);
            }
        }
        #endregion
    }
}
