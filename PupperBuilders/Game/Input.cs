﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PupperBuilders
{
    /// <summary>
    /// Handles all input in the game.
    /// </summary>
    static class Input
    {
        private static Form form;
        public static Form Form
        {
            set
            {
                if (form == null)
                {
                    form = value;
                }
            }
        }

        /// <summary>
        /// The different states the key and mouse can be.
        /// </summary>
        private enum InputStates
        {
            None,
            Down, //Pressed this VERY FRAME
            Up, //Released this very frame
            Press //Pressed for more than one frame
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern short GetKeyState(int keyCode);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        /// <summary>
        /// List of all key states.
        /// </summary>
        private static Dictionary<Keys, InputStates> keys = new Dictionary<Keys, InputStates>();

        /// <summary>
        /// The left mouse button state.
        /// </summary>
        private static InputStates mouse = InputStates.None;

        private static readonly object mousePositionLock = new object();
        private static Vector mousePosition;
        public static Vector MousePosition
        {
            get
            {
                lock (mousePositionLock)
                {
                    return mousePosition;
                }
            }
            set
            {
                lock(mousePositionLock)
                {
                    mousePosition = value;
                }
            }
        }

        /// <summary>
        /// Returns true if the key was pressed down this frame.
        /// </summary>
        /// <param name="key">The key you want to check.</param>
        public static bool KeyDown(params Keys[] inputKeys)
        {
            if (!IsFocused())
                return false;

            foreach (var key in inputKeys)
            {
                if (keys[key] == InputStates.Down)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if the key has been released this frame.
        /// </summary>
        /// <param name="key">The key you want to check.</param>
        public static bool KeyUp(params Keys[] inputKeys)
        {
            if (!IsFocused())
                return false;

            foreach (var key in inputKeys)
            {
                if (keys[key] == InputStates.Up)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if the key is down this frame.
        /// </summary>
        /// <param name="key">The key you want to check.</param>
        public static bool Key(params Keys[] inputKeys)
        {
            if (!IsFocused())
                return false;

            foreach (var key in inputKeys)
            {
                if (keys[key] == InputStates.Press)
                    return true;
            }
            return false;
        }

        public static void FindMousePosition()
        {
            if (!form.IsDisposed)
            {
                form.Invoke(new Action(() =>
                {
                    Point point = form.PointToClient(new Point(Cursor.Position.X, Cursor.Position.Y));
                    Input.MousePosition = new Vector(point.X, point.Y);
                }));
            }
        }

        /// <summary>
        /// Returns true if the left mouse button was pressed down this frame.
        /// </summary>
        /// <returns></returns>
        public static bool MouseDown()
        {
            if (!IsFocused())
                return false;

            if (mouse == InputStates.Down)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if the left mouse button has been released this frame.
        /// </summary>
        public static bool MouseUp()
        {
            if (!IsFocused())
                return false;

            if (mouse == InputStates.Up)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Returns whether or not this process is focused.
        /// </summary>
        /// <returns></returns>
        private static bool IsFocused()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }

        /// <summary>
        /// Returns true if the left mouse button is down this frame.
        /// </summary>
        public static bool Mouse()
        {
            if (!IsFocused())
                return false;

            if (mouse == InputStates.Press)
            {
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Fills the keys array for setup
        /// </summary>
        public static void SetupInput()
        {
            //Get a list of all keys.
            ArrayList allKeys = new ArrayList(Enum.GetValues(typeof(Keys)));
            foreach (Keys key in allKeys)
            {
                //If the key has not yet been added to the list. Add it.
                if (!keys.ContainsKey(key))
                {
                    keys.Add(key, InputStates.None);
                }

            }
        }

        /// <summary>
        /// Updates the state of the keys for the current frame. Is called every frame.
        /// </summary>
        public static void FrameUpdate()
        {
            FindMousePosition();

            ArrayList allKeys = new ArrayList(Enum.GetValues(typeof(Keys)));
            for (int i = 0; i < allKeys.Count; i++)
            {
                //Get the current key we are checking.
                Keys key = (Keys)allKeys[i];

                //If the key is pressed down.
                short retVal = GetKeyState((int)key);
                if ((retVal & 0x8000) == 0x8000)
                {
                    //If the key was not down last frame. Set key state to down.
                    if (keys[key] == InputStates.None)
                    {
                        keys[key] = InputStates.Down;
                    }
                    //If the key was down last frame. Set key state to press.
                    else
                    {
                        keys[key] = InputStates.Press;
                    }
                }
                //If the key is not pressed down.
                else
                {
                    //If the key was pressed last frame. Set key state to up.
                    if (keys[key] == InputStates.Press || keys[key] == InputStates.Down)
                    {
                        keys[key] = InputStates.Up;
                    }
                    else
                    {
                        keys[key] = InputStates.None;
                    }
                }
            }

            //If the left mouse button is pressed down.
            if (Control.MouseButtons == MouseButtons.Left)
            {
                //If the left mouse button was not down last frame. Set the mouse state to down.
                if (mouse == InputStates.None)
                {
                    mouse = InputStates.Down;
                }
                //If the left mouse button was down last frame. Set the mouse state to press.
                else
                {
                    mouse = InputStates.Press;
                }
            }
            //If the left mouse button is not pressed down.
            else
            {
                //If the left mouse button was pressed last frame. Set the mouse state to up.
                if (mouse == InputStates.Press || mouse == InputStates.Down)
                {
                    mouse = InputStates.Up;
                }
                else
                {
                    mouse = InputStates.None;
                }
            }
        }
    }
}