﻿//Lavet af Daniel
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Class for handling randomness.
    /// </summary>
    static class Rng
    {
        /// <summary>
        /// An instantiated object of the Random class. By using this you don't have to instantiate one yourself every time.
        /// </summary>
        private static Random random = new Random();
        /// <summary>
        /// Returns a random integer between min (inclusive) and max (exclusive)
        /// </summary>
        public static int NextInt(int min, int max)
        {
            return random.Next(min, max);
        }
        /// <summary>
        /// Returns a random float between min (inclusive) and max (inclusive) with a random set of decimals
        /// </summary>
        public static float NextFloat(float min, float max)
        {
            return min + (float)random.NextDouble() * (max - min);
        }
        /// <summary>
        /// Returns a random vector with a length of 1
        /// </summary>
        public static Vector OnUnitCircle
        {
            get
            {
                Vector ret = Vector.Right;
                ret.Rotate(NextFloat(0f, 360f));
                return ret;
            }
        }
        /// <summary>
        /// Returns a random vector with a length greater than 0 and less or equal to 1
        /// </summary>
        public static Vector InsideUnitCircle
        {
            get
            {
                return OnUnitCircle * NextFloat(float.Epsilon, 1f);
            }
        }
    }
}
