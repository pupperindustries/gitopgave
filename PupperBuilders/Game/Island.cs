﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// An island background
    /// </summary>
    class Island : GameObject
    {
        /// <summary>
        /// The size of the island in width and height
        /// </summary>
        public const float IslandSize = 15f;
        public Island() : base(AssetManager.GetImage("island.png"), Vector.Zero, Vector.One * IslandSize, 0f)
        {
            layer = GameObjectLayer.Background;
        }
        protected override void Update()
        {
            Camera.Position = Vector.Zero;
            Vector UIOffset = Camera.ScreenPosToWorld(new Vector(UISideMenu.Width, 0));
            float MidInGameAreaX = (Camera.ScreenPosToWorld(new Vector(Engine.Window.Width - UISideMenu.Width, 0)) / 2).x;
            float MidInGameAreaY = (Camera.ScreenPosToWorld(new Vector(0, UIResourcesPanel.Height))).y;

            Camera.Position -= UIOffset + new Vector(MidInGameAreaX, Camera.Size.y / 2 + MidInGameAreaY / 2);

            base.Update();
        }
    }
}
