﻿//Lavet af Levi
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PupperBuilders
{
    /// <summary>
    /// Used to write/show debug information at runtime in a separate window.
    /// </summary>
    static class Debug
    {
        /// <summary>
        /// A reference to the open window.
        /// </summary>
        private static DebugWindow window;

        public static void Start()
        {
#if DEBUG
            OpenWindow();
#endif
        }

        /// <summary>
        /// Write a message to the debug window.
        /// </summary>
        /// <param name="text">The text to what show.</param>
        public static void Log(string text)
        {
#if DEBUG
            if (window != null)
                window.AddLine(text);
#endif
        }

        /// <summary>
        /// Clear the debug window for messages.
        /// </summary>
        public static void Clear()
        {
#if DEBUG
            if (window != null)
                window.Clear();
#endif
        }

        /// <summary>
        /// OPen the debug window.
        /// </summary>
        private static void OpenWindow()
        {
#if DEBUG
            if (window == null)
            {
                window = new DebugWindow();
                window.Show();
            }
#endif
        }
    }
}
