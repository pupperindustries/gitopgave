﻿//Lavet af Levi og Daniel
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    struct Vector
    {
        public float x, y;
        public float Length
        {
            get
            {
                return Magnitude;
            }
        }
        public float Angle
        {
            get
            {
                var returnAngle = Math.Atan2(this.y, this.x);
                var degrees = 180 * returnAngle / Math.PI;
                return (float)(360 + Math.Round(degrees)) % 360;
            }
        }
        public float Magnitude
        {
            get
            {
                return (float)(Math.Sqrt(MagnitudeSqr));
            }
        }
        public float MagnitudeSqr
        {
            get
            {
                return (this.x * this.x + this.y * this.y);
            }
        }
        public Vector Normalized
        {
            get
            {
                float mag = Magnitude;
                Vector vector2 = new Vector(x, y);
                if (Math.Abs(mag) < 1e-9)
                {
                    vector2.x = 0;
                    vector2.y = 0;
                }
                else
                {
                    vector2.x /= mag;
                    vector2.y /= mag;
                }
                return vector2;
            }
        }

        public Vector(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public void Normalize()
        {
            var newVector = this.Normalized;
            x = newVector.x;
            y = newVector.y;
        }
        public void MoveTowards(Vector to, float percent)
        {
            var lerpVector = Vector.Lerp(this, to, percent);
            x = lerpVector.x;
            y = lerpVector.y;
        }
        /// <summary>
        /// Linearly interpolates from a vector to another vector, up to a certain distance towards target vector
        /// </summary>
        public static Vector LerpDistance(Vector from, Vector to, float maxDistance)
        {
            Vector delta = to - from;
            if (delta.MagnitudeSqr < maxDistance * maxDistance)
                return to;
            return from + delta.Normalized * maxDistance;
        }
        public float CrossProduct(Vector other)
        {
            return x * other.y - y * other.x;
        }
        public float DotProduct(Vector other)
        {
            return x * other.x + y * other.y;
        }
        public void Rotate(float detrees)
        {
            float cos = (float)(Math.Cos(detrees * Math.PI / 180));
            float sin = (float)(Math.Sin(detrees * Math.PI / 180));
            float oldX = this.x;
            float oldY = this.y;
            this.x = oldX * cos - oldY * sin;
            this.y = oldX * sin + oldY * cos;
        }

        public override string ToString()
        {
            return "(" + this.x + ", " + this.y + ")";
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Vector Zero
        {
            get
            {
                return new Vector(0, 0);
            }
        }
        public static Vector One
        {
            get
            {
                return new Vector(1, 1);
            }
        }
        public static Vector Up
        {
            get
            {
                return new Vector(0, -1);
            }
        }
        public static Vector Down
        {
            get
            {
                return new Vector(0, 1);
            }
        }
        public static Vector Left
        {
            get
            {
                return new Vector(-1, 0);
            }
        }
        public static Vector Right
        {
            get
            {
                return new Vector(1, 0);
            }
        }
        /// <summary>
		/// Returns a vector of equal magnitude reflected around the normal vector
		/// </summary>
		public Vector Reflect(Vector inNormal)
        {
            //Shamelessly stolen from https://github.com/MattRix/UnityDecompiled/blob/master/UnityEngine/UnityEngine/Vector2.cs
            return inNormal * DotProduct(inNormal) * -2f + this;
        }
        /// <summary>
        /// Finds a difference vector, and returns the angle property
        /// </summary>
        public static float DiferenceVectorAngle(Vector vector1, Vector vector2)
        {
            return (float)(Math.Atan2(vector2.y - vector1.y, vector2.x - vector1.x) * 180 / Math.PI) + 180;
        }
        /// <summary>
        /// Projects this vector onto the specified vector
        /// </summary>
        /// <returns></returns>
        public Vector Project(Vector other)
        {
            return other.Normalized * (DotProduct(other) / other.Magnitude);
        }
        /// <summary>
        /// Returns the angle between the two specified vectors
        /// </summary>
        public static float AngleBetween(Vector a, Vector b)
        {
            throw new Exception("Vector.AngleBetween is not working. Returns NaN always");
            return (float)(Math.Acos(a.DotProduct(b) / (a.Magnitude * b.Magnitude)) * 180 / Math.PI) - 180;
        }
        public static float Distance(Vector vector1, Vector vector2)
        {
            return (float)(Math.Sqrt(Vector.DistanceSqr(vector1, vector2)));
        }
        public static float DistanceSqr(Vector vector1, Vector vector2)
        {
            var deltaX = vector1.x - vector2.x;
            var deltaY = vector1.y - vector2.y;
            return (deltaX * deltaX + deltaY * deltaY);
        }
        /// <summary>
        /// Linearly interpolates between two vectors.
        /// </summary>
        public static Vector Lerp(Vector start, Vector end, float percent)
        {
            percent = Math.Min(percent, 1);
            percent = Math.Max(percent, 0);

            var diff = end - start;

            return start + diff * percent;
        }

        public static Vector operator +(Vector vector1, Vector vector2)
        {
            return new Vector(vector1.x + vector2.x, vector1.y + vector2.y);
        }
        public static Vector operator -(Vector vector1)
        {
            return new Vector(-vector1.x, -vector1.y);
        }
        public static Vector operator -(Vector vector1, Vector vector2)
        {
            return vector1 + (-vector2);
        }
        /// <summary>
        /// Returns a new vector with the products of each multiplied
        /// </summary>
        public static Vector operator *(Vector vector1, Vector vector2)
        {
            return new Vector(vector1.x * vector2.x, vector1.y * vector2.y);
        }
        public static Vector operator *(Vector vector1, float value)
        {
            return new Vector(vector1.x * value, vector1.y * value);
        }
        /// <summary>
        /// Returns a new vector with the products as a quotient of vector1 and vector2
        /// </summary>
        public static Vector operator /(Vector vector1, Vector vector2)
        {
            return new Vector(vector1.x / vector2.x, vector1.y / vector2.y);
        }
        public static Vector operator /(Vector vector1, float value)
        {
            return new Vector(vector1.x / value, vector1.y / value);
        }
        public static bool operator ==(Vector vector1, Vector vector2)
        {
            return (vector1.x - vector2.x).IsZero() && (vector1.y - vector2.y).IsZero();
        }
        public static bool operator !=(Vector vector1, Vector vector2)
        {
            return !(vector1 == vector2);
        }
    }
}
