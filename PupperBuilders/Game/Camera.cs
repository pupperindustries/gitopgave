﻿//Lavet af Levi og Daniel
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    static class Camera
    {
        private static readonly object cameraLock = new object();
        /// <summary>
        /// The position of the camera
        /// </summary>
        public static Vector Position = Vector.Zero;
        public static Vector Center
        {
            get
            {
                lock (cameraLock)
                {
                    return Position + Size * 0.5f;
                }
            }
            set
            {
                lock (cameraLock)
                {
                    Position = value - Size * 0.5f;
                }
            }
        }
        /// <summary>
        /// The height of camera in game units.
        /// </summary>
        private static float height = 1;
        public static float Height
        {
            get
            {
                lock (cameraLock)
                {
                    return height;
                }
            }
            set
            {
                lock (cameraLock)
                {
                    Vector center = Center;
                    height = value;
                    Center = center;
                }
            }
        }
        public static Vector Size
        {
            get
            {
                lock (cameraLock)
                {
                    return new Vector(Height * ((float)Engine.Window.Width / Engine.Window.Height), Height);
                }
            }
        }
        /// <summary>
        /// The factor by which to scale the Camera to fill the height of the screen
        /// </summary>
        public static float DrawScale
        {
            get
            {
                lock (cameraLock)
                {
                    return Engine.Window.Height / Height;
                }
            }
        }
        public static Color backgroundColor = Color.AliceBlue;
        /// <summary>
        /// Converts a point in world space to the corrosponding pixel on screen
        /// </summary>
        public static Vector WorldPosToScreen(Vector position)
        {
            position = (position - Position) * DrawScale;
            return position;
        }
        /// <summary>
        /// Converts a pixel to the corrosponding world position
        /// </summary>
        public static Vector ScreenPosToWorld(Vector screen)
        {
            screen /= DrawScale;
            screen += Position;
            return screen;
        }
        /// <summary>
        /// Scales a vector from world space to the same vector on the screen
        /// </summary>
        public static Vector WorldSizeToScreen(Vector size)
        {
            return size * DrawScale;
        }
        /// <summary>
        /// Scales a float from world space to the same float on the screen
        /// </summary>
        public static float WorldSizeToScreen(float size)
        {
            return size * DrawScale;
        }
        /// <summary>
        /// Scales a float from screen space to the same float on the screen
        /// </summary>
        public static float ScreenSizeToWorld(float size)
        {
            return size / DrawScale;
        }
    }
}
