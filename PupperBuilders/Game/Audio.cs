﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IrrKlang;
using System.IO;

namespace PupperBuilders
{
    static class Audio
    {
        /// <summary>
        /// The irrKlang sound engines. This is an external class from irrKlang.NET4.dll
        /// </summary>
		private static ISoundEngine engine = new ISoundEngine();

        /// <summary>
        /// We use the Dictionary to assign names to audio files. This way we can play them later without having to write the file path again. We can just reference them by name.
        /// The first string is the name assigned to the audio file. The second string is the path to the audio file.
        /// </summary>
        private static Dictionary<string, string> assignedAudioFiles = new Dictionary<string, string>();

        /// <summary>
        /// List of audios playing on the game object.
        /// </summary>
        private static List<ISound> sounds = new List<ISound>();
        
        /// <summary>
        /// Assign a name to an audio file. So we can reference the audio file by name later.
        /// </summary>
        /// <param name="name">The name we want to use later.</param>
        /// <param name="path">The name of the audio file. Remember extension.</param>
        public static void AssignNameToAudioFile(string name, string path)
        {
            lock (assignedAudioFiles)
            {
                //If the list of assigned names does not contain the new name. Add it.
                if (!assignedAudioFiles.ContainsKey(name))
                {
                    assignedAudioFiles.Add(name, path);
                }
            }
        }

        /// <summary>
        /// Play some audio. First, it checks if the given name exists in the list of assigned name. If not, it tries to find it on the disk.
        /// Remember to use extension if you want to play it from the disk.
        /// </summary>
        /// <param name="name">Name of the audio you want to play.</param>
        /// <param name="volume">The volume of the audio. 1 = 100%</param>
        public static void Play(string name, bool loop = false, float volume = 1)
        {
            CheckFinishedSounds();
            //If the name has been assigned to an audio file. Play the assigned audio.
            lock (assignedAudioFiles)
            {
                if (assignedAudioFiles.ContainsKey(name))
                {
                    name = assignedAudioFiles[name];
                }
            }
            if (engine.SoundVolume < 0.001f)
                return;
            ISoundSource source = AssetManager.GetAudio(name);
            if (source == null)
                return;
            ISound sound = engine.Play2D(source, loop, false, false);
            if (sound == null)
                return;
            sound.Volume = volume;
        }

        /// <summary>
        /// Sets the master volume of the specified sound type
        /// </summary>
        public static void SetMasterVolume(float volume)
        {
            lock(engine) engine.SoundVolume = volume;
        }
        public static float GetMasterVolume()
        {
            lock (engine)
            {
                return engine.SoundVolume;
            }
        }

        /// <summary>
        /// Checks if any sounds are finished playing, and removes them from the list of sounds.
        /// </summary>
        private static void CheckFinishedSounds()
        {
            lock (sounds)
            {
                sounds.RemoveAll(sound => sound.Finished);
            }
        }

        /// <summary>
        /// Stop all sounds for all sound engines
        /// </summary>
		public static void StopAllSounds()
        {
            lock (engine)
            {
                engine.StopAllSounds();
            }
        }
    }
}
