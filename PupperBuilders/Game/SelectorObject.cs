﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PupperBuilders
{
    /// <summary>
    /// Class used to manage selection of units
    /// </summary>
    class SelectorObject : GameObject
    {
        /// <summary>
        /// The unit that is currently selected
        /// </summary>
        private Unit selectedUnit;

        /// <summary>
        /// The location that is currently selected
        /// </summary>
        private IslandLocation selectedLocation;

        private static SelectorObject i;
        public static SelectorObject I { get { return i; } }

        private static readonly object selectedObjectLock = new object();

        public SelectorObject() : base(AssetManager.GetImage("arrow.png"), Vector.Zero , Vector.One, 0f)
        {
            i = this;
            layer = GameObjectLayer.Indicator;
        }

        /// <summary>
        /// Determines what happens when a unit is selected
        /// </summary>
        /// <param name="unit"></param>
        public void SelectUnit(Unit unit)
        {
            lock (selectedObjectLock)
            {
                if (selectedUnit == unit)
                {
                    selectedUnit = null;
                }
                else
                {
                    selectedUnit = unit;
                }
            }
        }

        /// <summary>
        /// Determines what happens when a location is selected
        /// </summary>
        /// <param name="location"></param>
        public void SelectLocation(IslandLocation location)
        {
            lock (selectedObjectLock)
            {
                if (selectedUnit != null)
                {
                    selectedUnit.AssignLocation(location);
                    selectedUnit.CalculateSubtarget();
                    selectedUnit = null;
                }
            }
        }

        protected override void Update()
        {
            lock (selectedObjectLock)
            {
                if (selectedUnit != null)
                {
                    Position = Camera.ScreenPosToWorld(selectedUnit.uIHealthIndicator.Position) + Vector.Up * 0.4f; 
                    Size = Vector.One * 0.5f;
                }
                else
                {
                    Size = Vector.Zero;
                }
            }
            base.Update();
        }
    }
}
