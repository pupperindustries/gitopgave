﻿//Lavet af Levi
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PupperBuilders
{
    public partial class DebugWindow : Form
    {
        public DebugWindow()
        {
            InitializeComponent();
        }
        private void button_clear_Click(object sender, EventArgs e)
        {
            Clear();
        }
        public void Clear()
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action(Clear), new object[] {});
                return;
            }
            debugList.Text = "";
        }
        public void AddLine(string text)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AddLine), new object[] { text });
                return;
            }
            debugList.Text += text + "\r\n";
        }
    }
}
