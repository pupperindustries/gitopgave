﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PupperBuilders
{
    public partial class Form1 : Form
    {
        Engine engine;
        public Form1()
        {
            InitializeComponent();
            Text = "Pupper Builders 1.1.1";
            engine = new Engine(this, CreateGraphics(), DisplayRectangle);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            engine.CloseEngine();
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            if (engine == null)
                return;

            engine.SetGraphics(CreateGraphics(), DisplayRectangle);
        }

        private FormWindowState formWindowState;
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (formWindowState != WindowState)
            {
                formWindowState = WindowState;
                engine.SetGraphics(CreateGraphics(), DisplayRectangle);
            }
        }
    }
}
